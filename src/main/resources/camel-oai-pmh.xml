<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:xsl="http://camel.apache.org/schema/spring"
       xsi:schemaLocation="
       http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
       http://camel.apache.org/schema/spring http://camel.apache.org/schema/spring/camel-spring.xsd">

    <!-- spring property placeholder -->
    <context:property-placeholder location="classpath:camel-oai-pmh.properties"/>

    <!-- data source for storage of harvester instances -->
    <bean id="dataSource" class="org.apache.commons.dbcp2.BasicDataSource" destroy-method="close">
        <property name="driverClassName" value="${db.driver}"/>
        <property name="url" value="${db.url}"/>
        <property name="username" value="${db.username}"/>
        <property name="password" value="${db.password}"/>
        <property name="maxTotal" value="10"/>
        <property name="maxIdle" value="1"/>
    </bean>

    <!-- configure the Camel SQL component to use the JDBC data source -->
    <bean id="sql" class="org.apache.camel.component.sql.SqlComponent">
        <property name="dataSource" ref="dataSource"/>
    </bean>

    <!-- Sets parameters in URL template for for OpenSearch requests -->
    <bean id="osParameterProcessor" class="eu.odp.harvest.geo.oai.OsParameterProcessor"/>

    <bean id="exceptionProcessor" class="eu.odp.harvest.geo.oai.ExceptionProcessor"/>

    <!-- management bean used for CRUD operations on harvesters -->
    <bean id="harvesterManager" class="eu.odp.harvest.geo.oai.HarvesterManager"/>

    <!-- Camel context for this application -->
    <camelContext xmlns="http://camel.apache.org/schema/spring"
                  xmlns:oai="http://www.openarchives.org/OAI/2.0/">
        <!-- internal endpoint of processing route for INSPIRE catalogs -->
        <endpoint id="inspire" uri="direct:inspire"/>
        <!-- internal endpoint of processing route for INSPIRE catalogs via SOAP 1.1 -->
        <endpoint id="inspireSoap11" uri="direct:inspireSoap11"/>
        <!-- internal endpoint of processing route for INSPIRE catalogs via SOAP 1.2 -->
        <endpoint id="inspireSoap12" uri="direct:inspireSoap12"/>
        <!-- internal endpoint of processing route for ESA EOP OpenSearch -->
        <endpoint id="esaEopos" uri="direct:esaEopos"/>

        <!-- endpoint for harvester type management -->
        <endpoint id="mgmtHarvesterTypes" uri="servlet:///mgmt/harvester-types"/>

        <!-- endpoint for harvester management -->
        <endpoint id="mgmtHarvesters" uri="servlet:///mgmt/harvesters?matchOnUriPrefix=true"/>

        <!-- endpoint for harvester management -->
        <endpoint id="harvesters" uri="servlet:///harvesters?matchOnUriPrefix=true"/>

        <!-- utility endpoint used for harvester creation/update -->
        <endpoint id="harvesterParams" uri="direct://harvesterParams"/>

        <!-- JAXB marshalling of harvesters -->
        <dataFormats>
            <jaxb id="jaxb" prettyPrint="true" contextPath="eu.odp.harvest.geo.oai"/>
        </dataFormats>

        <onException>
            <exception>java.lang.Exception</exception>
            <handled>
                <constant>true</constant>
            </handled>
            <log loggingLevel="ERROR" message="${exception.stacktrace}" logName="error"/>
            <to uri="bean:exceptionProcessor"/>
        </onException>

        <!-- generic route for all harvesters, the parameters for the requested harvester are loaded from the database -->
        <route>
            <from uri="ref:harvesters"/>
            <setBody>
                <simple>${header.CamelHttpPath.replaceAll('/', '')}</simple>
            </setBody>
            <to uri="log:input"/>
            <setHeader name="harvesterId">
                <simple>${body}</simple>
            </setHeader>
            <setHeader name="harvester_base_url">
                <simple>${header.CamelHttpUrl}</simple>
            </setHeader>
            <to uri="sql:select type,url,type_filter_overwrite from harvesters where id = #"/>
            <choice>
                <when>
                    <simple>${body.size} &gt; 0</simple>
                    <setHeader name="typeFilterOverwrite">
                        <simple>${body[0][TYPE_FILTER_OVERWRITE]}</simple>
                    </setHeader>
                    <setHeader name="CamelHttpUri">
                        <simple>${body[0][URL]}</simple>
                    </setHeader>
                    <setHeader name="CamelHttpPath">
                        <constant/>
                    </setHeader>
                    <recipientList>
                        <simple>direct:${body[0][TYPE]}</simple>
                    </recipientList>
                </when>
                <otherwise>
                    <setHeader name="CamelHttpResponseCode">
                        <constant>404</constant>
                    </setHeader>
                </otherwise>
            </choice>
        </route>

        <!-- route for INSPIRE catalogs -->
        <route>
            <from uri="ref:inspire"/>
            <setHeader name="response_date">
                <simple>${date:now:yyyy-MM-dd'T'HH:mm:ss'Z'}</simple>
            </setHeader>
            <!-- we need some element for xsl input, but the result only depends on header (request) parameters -->
            <setBody>
                <constant>&lt;dummy/&gt;</constant>
            </setBody>
            <to uri="xslt:inspireRequest.xsl"/>
            <xsl:choice>
                <when>
                    <simple>${header.harvesterId} == 'ices'</simple>
                    <to uri="xslt:icesRequest.xsl"/>
                </when>
            </xsl:choice>
            <convertBodyTo type="java.lang.String"/>
            <to uri="log:input"/>
            <setHeader name="harvestingCswRequest">
                <simple>${body}</simple>
            </setHeader>
            <choice>
                <when>
                    <!-- Only send a GetRecords request to the target catalog if we have not already created an OAI-PMH response -->
                    <xpath>count(/oai:OAI-PMH) = 0</xpath>
                    <setHeader name="CamelHttpMethod">
                        <constant>POST</constant>
                    </setHeader>
                    <choice>
                        <when>
                            <simple>${header.soapVersion} == '1.1'</simple>
                            <setHeader name="Content-Type">
                                <constant>text/xml</constant>
                            </setHeader>
                        </when>
                        <when>
                            <simple>${header.soapVersion} == '1.2'</simple>
                            <setHeader name="Content-Type">
                                <constant>application/soap+xml</constant>
                            </setHeader>
                        </when>
                        <otherwise>
                            <setHeader name="Content-Type">
                                <constant>application/xml</constant>
                            </setHeader>
                        </otherwise>
                    </choice>
                    <removeHeader name="CamelHttpQuery"/>
                    <removeHeader name="harvestingCswRequest"/>
                    <!-- Replaced dynamically with the target catalog URL -->
                    <to uri="http://dummy?httpClient.SocketTimeout=180000&amp;httpClient.cookieSpec=ignoreCookies"/>
                    <convertBodyTo type="java.lang.String"/>
                    <!-- transform the GetRecordsResponse -->
                    <to uri="xslt:inspireResponse.xsl"/>
                </when>
            </choice>
            <setHeader name="Content-Type">
                <constant>text/xml</constant>
            </setHeader>
        </route>

        <!-- route for INSPIRE catalogs via SOAP 1.1 -->
        <route>
            <from uri="ref:inspireSoap11"/>
            <setHeader name="soapVersion">
                <simple>1.1</simple>
            </setHeader>
            <setHeader name="SOAPAction">
                <constant>http://www.opengis.net/cat/csw/2.0.2/requests#GetRecords</constant>
            </setHeader>
            <to uri="ref:inspire"/>
        </route>

        <!-- route for INSPIRE catalogs via SOAP 1.2 -->
        <route>
            <from uri="ref:inspireSoap12"/>
            <setHeader name="soapVersion">
                <simple>1.2</simple>
            </setHeader>
            <setHeader name="SOAPAction">
                <constant>http://www.opengis.net/cat/csw/2.0.2/requests#GetRecords</constant>
            </setHeader>
            <to uri="ref:inspire"/>
        </route>

        <!-- Route for ESA EOP OpenSearch -->
        <route>
            <from uri="ref:esaEopos"/>
            <setHeader name="response_date">
                <simple>${date:now:yyyy-MM-dd'T'HH:mm:ss'Z'}</simple>
            </setHeader>
            <!-- we need some element for xsl input, but the result only depends on header (request) parameters -->
            <setBody>
                <constant>&lt;dummy/&gt;</constant>
            </setBody>
            <to uri="xslt:esa-eoposRequest.xsl"/>
            <to uri="log:input"/>
            <choice>
                <when>
                    <!-- Only send an OpenSearch request to the target catalog if we have not already created an OAI-PMH response -->
                    <xpath>count(/oai:OAI-PMH) = 0</xpath>
                    <setHeader name="CamelHttpMethod">
                        <constant>GET</constant>
                    </setHeader>
                    <choice>
                        <when>
                            <simple>${header.CamelHttpUri} contains 'startPage?'</simple>
                            <setBody>
                                <xpath>/parameters/parameter[name='startPage']/value/text()</xpath>
                            </setBody>
                        </when>
                        <otherwise>
                            <setBody>
                                <xpath>/parameters/parameter[name='startIndex']/value/text()</xpath>
                            </setBody>
                        </otherwise>
                    </choice>
                    <convertBodyTo type="java.lang.String"/>
                    <to uri="bean:osParameterProcessor"/>
                    <!-- Replaced dynamically with the target catalog URL -->
                    <to uri="http://dummy"/>
<!--
                    <convertBodyTo type="java.lang.String"/>
                    <to uri="log:input"/>
-->
                    <!-- transform the GetRecordsResponse -->
                    <to uri="xslt:esa-eoposResponse.xsl"/>
                </when>
            </choice>
            <setHeader name="Content-Type">
                <constant>text/xml</constant>
            </setHeader>
        </route>

        <!-- management route for harvester types -->
        <route>
            <from uri="ref:mgmtHarvesterTypes"/>
            <setBody>
                <simple>resource:classpath:/harvester-types.xml</simple>
            </setBody>
            <setHeader name="Content-Type">
                <constant>text/xml</constant>
            </setHeader>
        </route>

        <!-- management route for harvesters -->
        <route>
            <from uri="ref:mgmtHarvesters"/>
            <setHeader name="Content-Type">
                <constant>text/xml</constant>
            </setHeader>
            <doTry>
                <choice>
                    <when>
                        <simple>${header.CamelHttpMethod} == 'GET'</simple>
                        <setBody>
                            <simple>${header.CamelHttpPath.replaceAll('/', '')}</simple>
                        </setBody>
                        <to uri="log:input"/>
                        <choice>
                            <when>
                                <simple>${body} == ''</simple>
                                <to uri="sql:select * from harvesters"/>
                                <to uri="bean:harvesterManager?method=createHarvesters(${body}, ${header.CamelHttpUrl})"/>
                            </when>
                            <otherwise>
                                <to uri="sql:select * from harvesters where id = #"/>
                                <to uri="bean:harvesterManager?method=createHarvester(${body}, ${header.CamelHttpUrl})"/>
<!--
                                <choice>
                                    <when>
                                        <simple>${body.size} == 0</simple>
                                        <setHeader name="CamelHttpResponseCode">
                                            <constant>404</constant>
                                        </setHeader>
                                    </when>
                                    <otherwise>
                                        <to uri="bean:harvesterManager?method=createHarvester(${body})"/>
                                    </otherwise>
                                </choice>
-->
                            </otherwise>
                        </choice>
                        <marshal>
                            <custom ref="jaxb"/>
                        </marshal>
                    </when>
                    <when>
                        <simple>${header.CamelHttpMethod} == 'PUT'</simple>
                        <to uri="ref:harvesterParams"/>
                        <to uri="sql:update harvesters set type = :#harvesterType, name = :#harvesterName, description = :#harvesterDescription, url = :#harvesterUrl, selective= :#harvesterSelective, responsibleparty= :#harvesterResponsibleParty, email= :#harvesterEmail, homepage= :#harvesterHomepage, language= :#harvesterLanguage, country= :#harvesterCountry, type_filter_overwrite= :#harvesterTypeFilterOverwrite where id = :#harvesterId"/>
                        <choice>
                            <when>
                                <simple>${header.CamelSqlUpdateCount} == 0</simple>
                                <setHeader name="CamelHttpResponseCode">
                                    <constant>404</constant>
                                </setHeader>
                            </when>
                        </choice>
                        <setBody>
                            <constant/>
                        </setBody>
                    </when>
                    <when>
                        <simple>${header.CamelHttpMethod} == 'POST'</simple>
                        <to uri="ref:harvesterParams"/>
                        <to uri="sql:select id from harvesters where id = :#harvesterId"/>
                        <choice>
                            <when>
                                <simple>${body.size} == 0</simple>
                                <to uri="sql:insert into harvesters values(:#harvesterId, :#harvesterType, :#harvesterName, :#harvesterDescription, :#harvesterUrl, :#harvesterSelective, :#harvesterResponsibleParty, :#harvesterEmail, :#harvesterHomepage, :#harvesterLanguage, :#harvesterCountry, :#harvesterTypeFilterOverwrite)"/>
                                <setHeader name="CamelHttpResponseCode">
                                    <constant>201</constant>
                                </setHeader>
                                <setHeader name="Location">
                                    <simple>${header.CamelHttpUrl}/${header.harvesterId}</simple>
                                </setHeader>
                            </when>
                            <otherwise>
                                <setHeader name="CamelHttpResponseCode">
                                    <constant>409</constant>
                                </setHeader>
                                <setBody>
                                    <constant>&lt;error&gt;A harvester with this id already exists&lt;/error&gt;</constant>
                                </setBody>
                            </otherwise>
                        </choice>
                        <setBody>
                            <constant/>
                        </setBody>
                    </when>
                    <when>
                        <simple>${header.CamelHttpMethod} == 'DELETE'</simple>
                        <setBody>
                            <simple>${header.CamelHttpPath.replaceAll('/', '')}</simple>
                        </setBody>
                        <to uri="sql:delete from harvesters where id = #"/>
                        <choice>
                            <when>
                                <simple>${header.CamelSqlUpdateCount} == 0</simple>
                                <setHeader name="CamelHttpResponseCode">
                                    <constant>404</constant>
                                </setHeader>
                            </when>
                        </choice>
                        <setBody>
                            <constant/>
                        </setBody>
                    </when>
                    <otherwise>
                        <setHeader name="CamelHttpResponseCode">
                            <constant>405</constant>
                        </setHeader>
                        <setHeader name="Allow">
                            <constant>GET, POST, PUT, DELETE</constant>
                        </setHeader>
                        <setBody>
                            <constant>&lt;error&gt;Invalid method&lt;/error&gt;</constant>
                        </setBody>
                    </otherwise>
                </choice>
                <doCatch>
                    <exception>eu.odp.harvest.geo.oai.ManagerException</exception>
                    <setHeader name="CamelHttpResponseCode">
                        <simple>${exception.statusCode}</simple>
                    </setHeader>
                    <setBody>
                        <simple>&lt;error&gt;${exception.message}&lt;/error&gt;</simple>
                    </setBody>
                </doCatch>
            </doTry>
            <removeHeaders pattern="harvester*"/>
        </route>

        <!-- utility route used for harvester creation/update -->
        <route>
            <from uri="ref:harvesterParams"/>
            <setHeader name="harvesterId">
                <xpath resultType="java.lang.String">/harvester/id/text()</xpath>
            </setHeader>
            <setHeader name="harvesterType">
                <xpath resultType="java.lang.String">/harvester/type/text()</xpath>
            </setHeader>
            <setHeader name="harvesterName">
                <xpath resultType="java.lang.String">/harvester/name/text()</xpath>
            </setHeader>
            <setHeader name="harvesterDescription">
                <xpath resultType="java.lang.String">/harvester/description/text()</xpath>
            </setHeader>
            <setHeader name="harvesterUrl">
                <xpath resultType="java.lang.String">/harvester/url/text()</xpath>
            </setHeader>
            <setHeader name="harvesterSelective">
                <xpath resultType="java.lang.String">/harvester/selective/text()</xpath>
            </setHeader>
            <setHeader name="harvesterResponsibleParty">
                <xpath resultType="java.lang.String">/harvester/responsibleParty/text()</xpath>
            </setHeader>
            <setHeader name="harvesterEmail">
                <xpath resultType="java.lang.String">/harvester/email/text()</xpath>
            </setHeader>
            <setHeader name="harvesterHomepage">
                <xpath resultType="java.lang.String">/harvester/homepage/text()</xpath>
            </setHeader>
            <setHeader name="harvesterLanguage">
                <xpath resultType="java.lang.String">/harvester/language/text()</xpath>
            </setHeader>
            <setHeader name="harvesterCountry">
                <xpath resultType="java.lang.String">/harvester/country/text()</xpath>
            </setHeader>
            <setHeader name="harvesterTypeFilterOverwrite">
                <xpath resultType="java.lang.String">/harvester/typeFilterOverwrite/text()</xpath>
            </setHeader>
            <choice>
                <when>
                    <simple>${header.harvesterSelective} == 'true'</simple>
                    <setHeader name="harvesterSelective">
                        <constant>1</constant>
                    </setHeader>
                </when>
                <otherwise>
                    <setHeader name="harvesterSelective">
                        <constant>0</constant>
                    </setHeader>
                </otherwise>
            </choice>
            <to uri="bean:harvesterManager?method=validateParams(${header.harvesterId}, ${header.harvesterType}, ${header.harvesterUrl})"/>
        </route>
    </camelContext>

</beans>