<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gml="http://www.opengis.net/gml"
                xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:dct="http://purl.org/dc/terms/"
                xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gmx="http://www.isotc211.org/2005/gmx"
                xmlns:dcat="http://www.w3.org/ns/dcat#" xmlns:foaf="http://xmlns.com/foaf/0.1/"
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                xmlns:locn="http://www.w3.org/ns/locn#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:vcard="http://www.w3.org/2006/vcard/ns#"
                xmlns:prov="http://www.w3.org/ns/prov#" xmlns:org="http://www.w3.org/ns/org#"
                xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:owl="http://www.w3.org/2002/07/owl#"
                xmlns:cnt="http://www.w3.org/2011/content#" xmlns:adms="http://www.w3.org/ns/adms#"
                xmlns:gmi="http://www.isotc211.org/2005/gmi" xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:dqv="http://www.w3.org/ns/dqv#" xmlns:geodcatap="http://data.europa.eu/930/"
                xmlns:sdmx-attribute="http://purl.org/linked-data/sdmx/2009/attribute#"
                exclude-result-prefixes="gmd gco srv xlink gmi">

    <xsl:output method="xml"/>
    
    <!--
    <xsl:strip-space elements="*"/>
    -->
    
    <xsl:param name="resumptionToken">51:dcat_ap::</xsl:param>
    <xsl:variable name="tokeDcatAp">:dcat_ap:</xsl:variable>
    <xsl:param name="metadataPrefix">dcat_ap</xsl:param>
    <xsl:variable name="prefixDcatAp">dcat_ap</xsl:variable>
    <xsl:variable name="extended" select="($metadataPrefix and $metadataPrefix != $prefixDcatAp) or ($resumptionToken and not(contains($resumptionToken, $tokeDcatAp)))"/>
    
    <xsl:variable name="inspireThemes" select="document('themes.rdf')"/>
    <!-- EuroVoc mapping deactivated
    <xsl:variable name="euroVocMapping" select="document('align_EuroVoc_Inspire.rdf')"/>
    -->
    <xsl:variable name="mdrFileTypes" select="document('filetypes-skos.rdf')"/>
    <xsl:variable name="ianaMediaTypes" select="document('iana-media-types.xml')"/>
    
    <xsl:variable name="inspire_md_codelist">http://inspire.ec.europa.eu/metadata-codelist/</xsl:variable>
    <xsl:variable name="epsgRegister">http://www.opengis.net/def/crs/EPSG/0</xsl:variable>

    <xsl:template match="gmd:MD_Metadata|gmi:MI_Metadata">
        <dcat:Dataset>
            <xsl:call-template name="commonProperties"/>
            <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:descriptiveKeywords/*/gmd:keyword/gco:CharacterString[text()]"/>
            <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:language/*[string-length(@codeListValue) = 3]|gmd:identificationInfo/*/gmd:language/gco:CharacterString[string-length(text()) = 3]"/>
            <xsl:apply-templates select="gmd:dataQualityInfo/*/gmd:lineage/*/gmd:statement/gco:CharacterString[text()]"/>
            <!-- now services are handled the same way as datasets -->
            <xsl:choose>
                <xsl:when test="gmd:hierarchyLevel/*/@codeListValue = 'service'">
                    <xsl:call-template name="serviceDistribution"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="dataDistribution"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:citation/*/gmd:identifier/*/gmd:code/gco:CharacterString"/>
            <xsl:if test="not(gmd:identificationInfo[1]/*/gmd:descriptiveKeywords/*[starts-with(gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString, 'GEMET - INSPIRE themes')]/gmd:keyword/gco:CharacterString[text()])">
                <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:topicCategory/*" mode="dcatTheme"/>
            </xsl:if>
            <xsl:apply-templates select="gmd:parentIdentifier/*[text()]"/>
            <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:spatialResolution/*/gmd:equivalentScale/*/gmd:distance/*"/>
            <xsl:if test="$extended">
                <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:topicCategory/*"/>
                <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:spatialResolution/*/gmd:equivalentScale/*/gmd:denominator/*[. castable as xs:double]"/>
            </xsl:if>
        </dcat:Dataset>
    </xsl:template>
    
    <xsl:template match="gmd:denominator/*">
        <dqv:hasQualityMeasurement>
            <dqv:QualityMeasurement>
                <dqv:isMeasurementOf>
                    <dqv:Metric rdf:about="http://data.europa.eu/930/spatialResolutionAsScale"/>
                </dqv:isMeasurementOf>
                <dqv:value rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">
                    <xsl:value-of select="format-number((1 div .), '0.########################')"/>
                </dqv:value>
            </dqv:QualityMeasurement>
        </dqv:hasQualityMeasurement>
    </xsl:template>
    
    <xsl:template match="gmd:distance/*">
        <xsl:variable name="unit">
            <xsl:choose>
                <xsl:when test="@uom = 'EPSG::9001' or @uom = 'urn:ogc:def:uom:EPSG::9001' or @uom = 'urn:ogc:def:uom:UCUM::m' or @uom = 'urn:ogc:def:uom:OGC::m'">
                    <xsl:value-of select="concat('m',' (',@uom,')')"/>
                </xsl:when>
                <xsl:when test="@uom = 'EPSG::9002' or @uom = 'urn:ogc:def:uom:EPSG::9002' or @uom = 'urn:ogc:def:uom:UCUM::[ft_i]' or @uom = 'urn:ogc:def:uom:OGC::[ft_i]'">
                    <xsl:value-of select="concat('ft',' (',@uom,')')"/>
                </xsl:when>
                <xsl:when test="starts-with(@uom, 'http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/uom/ML_gmxUom.xml#')">
                    <xsl:value-of select="concat(substring-after(@uom, 'http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/uom/ML_gmxUom.xml#'),' (',@uom,')')"/>
                </xsl:when>
                <!-- To be completed -->
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space(@uom)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test=". castable as xs:double and (starts-with($unit, 'm') or starts-with($unit, 'km') or starts-with($unit, 'ft'))">
                <xsl:variable name="value" select="if (starts-with($unit, 'km')) then . * 1000 else (if (starts-with($unit, 'f')) then . * 0.3048 else .)"/>
                <dcat:spatialResolutionInMeters rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">
                    <xsl:value-of select="$value"/>
                </dcat:spatialResolutionInMeters>
                <xsl:if test="$extended">
                    <dqv:hasQualityMeasurement>
                        <dqv:QualityMeasurement>
                            <dqv:isMeasurementOf>
                                <dqv:Metric rdf:about="http://data.europa.eu/930/spatialResolutionAsDistance"/>
                            </dqv:isMeasurementOf>
                            <dqv:value rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal"><xsl:value-of select="."/></dqv:value>
                            <sdmx-attribute:unitMeasure rdf:resource="{if (starts-with($unit, 'km')) then 'http://www.qudt.org/vocab/unit/KiloM' else (if (starts-with($unit, 'f')) then 'http://www.qudt.org/vocab/unit/FT' else 'http://www.qudt.org/vocab/unit/M')}"/>
                        </dqv:QualityMeasurement>
                    </dqv:hasQualityMeasurement>
                </xsl:if>
            </xsl:when>
            <xsl:when test="$extended">
                <rdfs:comment xml:lang="en">
                    <xsl:value-of select="concat('Spatial resolution (distance): ', text(), ' ', @uom)"/>
                </rdfs:comment>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="commonProperties">
        <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:citation/*/gmd:title/gco:CharacterString[text()]"/>
        <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:abstract/gco:CharacterString[text()]"/>
        <xsl:apply-templates select="gmd:identificationInfo[1]/*/*:extent/*/gmd:geographicElement"/>
        <xsl:apply-templates select="gmd:identificationInfo[1]/*/*:extent/*/gmd:temporalElement/*/gmd:extent/gml:TimePeriod[gml:beginPosition/text() castable as xs:date or gml:beginPosition/text() castable as xs:dateTime or gml:endPosition/text() castable as xs:date or gml:endPosition/text() castable as xs:dateTime]"/>
        <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:citation/*/gmd:date/*[gmd:dateType/*/@codeListValue = 'publication' or gmd:dateType/*/@codeListValue = 'revision']/gmd:date/*"/>
        <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:pointOfContact/*[gmd:organisationName/*/text() or gmd:individualName/text()]"/>
        <xsl:apply-templates select="gmd:dataQualityInfo/*/gmd:report/*/gmd:result/gmd:DQ_ConformanceResult/gmd:specification/gmd:CI_Citation"/>
        <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:resourceMaintenance/*/gmd:maintenanceAndUpdateFrequency/*/@codeListValue"/>
        <xsl:if test="$extended">
            <xsl:apply-templates select="gmd:identificationInfo[1]/*/gmd:citation/*/gmd:date/*[gmd:dateType/*/@codeListValue = 'creation']/gmd:date/*"/>
            <foaf:isPrimaryTopicOf>
                <rdf:Description>
                    <rdf:type rdf:resource="http://www.w3.org/ns/dcat#CatalogRecord"/>
                    <dct:conformsTo>
                        <dct:Standard rdf:about="{if ($extended) then 'http://data.europa.eu/930/' else 'http://data.europa.eu/r5r/'}"/>
                    </dct:conformsTo>
                    <xsl:apply-templates select="gmd:language/*[@codeListValue]|gmd:language/gco:CharacterString"/>
                    <xsl:apply-templates select="gmd:dateStamp/*"/>
                    <xsl:apply-templates select="gmd:contact/*[gmd:organisationName/*/text() or gmd:individualName/*/text()]"/>
                    <xsl:apply-templates select="gmd:fileIdentifier/gco:CharacterString"/>
                    <dct:source rdf:parseType="Resource">
                        <rdf:type rdf:resource="http://www.w3.org/ns/dcat#CatalogRecord"/>
                        <xsl:apply-templates select="gmd:dateStamp/*"/>
                        <xsl:apply-templates select="gmd:characterSet/*/@codeListValue"/>
                        <dct:conformsTo rdf:parseType="Resource">
                            <rdf:type rdf:resource="http://purl.org/dc/terms/Standard"/>
                            <dct:title xml:lang="de">ISO 19115, ISO 19119</dct:title>
                            <owl:versionInfo xml:lang="de">ISO 19115 2003, ISO 19119 2006</owl:versionInfo>
                        </dct:conformsTo>
                    </dct:source>
<!--
                    <xsl:apply-templates select="gmd:metadataStandardName/gco:CharacterString"/>
                    <xsl:apply-templates select="gmd:characterSet/*/@codeListValue"/>
                    -->
                </rdf:Description>
            </foaf:isPrimaryTopicOf>
            <xsl:apply-templates select="gmd:referenceSystemInfo/*/gmd:referenceSystemIdentifier/*[gmd:code/gco:CharacterString != '' or gmd:code/gmx:Anchor/@xlink:href != '']"/>
            <xsl:apply-templates select="gmd:hierarchyLevel"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="gmd:fileIdentifier/*">
        <dct:identifier rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
            <xsl:value-of select="."/>
        </dct:identifier>
    </xsl:template>
    
    <xsl:template match="gmd:metadataStandardName/gco:CharacterString">
        <dct:source rdf:parseType="Resource">
            <dct:conformsTo rdf:parseType="Resource">
                <dct:title>
                    <xsl:call-template name="xmlLang"/>
                    <xsl:value-of select="."/>
                </dct:title>
                <xsl:apply-templates select="../../gmd:metadataStandardVersion/*"/>
            </dct:conformsTo>
        </dct:source>
    </xsl:template>
    
    <xsl:template match="gmd:metadataStandardName/gmx:Anchor">
        <dct:source rdf:parseType="Resource">
            <dct:conformsTo rdf:resource="{iri-to-uri(@xlink:href)}"/>
        </dct:source>
    </xsl:template>

    <xsl:template match="gmd:metadataStandardVersion/*">
        <owl:versionInfo>
            <xsl:call-template name="xmlLang"/>
            <xsl:value-of select="."/>
        </owl:versionInfo>
    </xsl:template>
    
    <xsl:template match="gmd:specification/gmd:CI_Citation">
        <xsl:if test="$extended">
            <prov:wasUsedBy>
                <prov:Activity>
                    <prov:qualifiedAssociation rdf:parseType="Resource">
                        <rdf:type rdf:resource="http://www.w3.org/ns/prov#Association"/>
                        <prov:hadPlan rdf:parseType="Resource">
                            <rdf:type rdf:resource="http://www.w3.org/ns/prov#Plan"/>
                            <prov:wasDerivedFrom>
                                <xsl:apply-templates mode="specinfo" select="."/>
                            </prov:wasDerivedFrom>
                        </prov:hadPlan>
                    </prov:qualifiedAssociation>
                    <prov:generated rdf:parseType="Resource">
                        <rdf:type rdf:resource="http://www.w3.org/ns/prov#Entity"/>
                        <dct:type>
                            <xsl:attribute name="rdf:resource">
                                <xsl:value-of select="concat($inspire_md_codelist, 'DegreeOfConformity/')"/>
                                <xsl:variable name="pass" select="../../gmd:pass/gco:Boolean"/>
                                <xsl:choose>
                                    <xsl:when test="$pass = 'true'">conformant</xsl:when>
                                    <xsl:when test="$pass = 'false'">notConformant</xsl:when>
                                    <xsl:otherwise>notEvaluated</xsl:otherwise>
                                </xsl:choose>
                            </xsl:attribute>
                        </dct:type>
                        <xsl:apply-templates select="../../gmd:explanation/*"/>
                    </prov:generated>
                </prov:Activity>
            </prov:wasUsedBy>
        </xsl:if>
        <xsl:apply-templates select="." mode="conformsTo"/>
    </xsl:template>

    <xsl:template match="gmd:explanation/*">
        <dct:description>
            <xsl:call-template name="xmlLang"/>
            <xsl:value-of select="text()"/>
        </dct:description>
    </xsl:template>
    
    <xsl:template match="gmd:CI_Citation" mode="conformsTo"/>
    
    <xsl:template match="gmd:CI_Citation[../../gmd:pass/gco:Boolean = 'true']" mode="conformsTo">
        <dct:conformsTo>
            <xsl:apply-templates select="." mode="specinfo">
                <xsl:with-param name="rdfType" select="'http://purl.org/dc/terms/Standard'"/>
            </xsl:apply-templates>
        </dct:conformsTo>
    </xsl:template>

    <xsl:template match="gmd:specification/gmd:CI_Citation" mode="specinfo">
        <xsl:param name="rdfType" select="'http://www.w3.org/ns/prov#Entity'" as="xs:string"/>
        <xsl:attribute name="rdf:parseType">Resource</xsl:attribute>
        <rdf:type rdf:resource="{$rdfType}"/>
        <xsl:call-template name="specinfo"/>
    </xsl:template>
    
    <xsl:template match="gmd:specification/gmd:CI_Citation[gmd:title/gmx:Anchor/@xlink:href != '']" mode="specinfo" priority="4.0">
        <prov:Entity rdf:about="{iri-to-uri(gmd:title/gmx:Anchor/@xlink:href)}"/>
    </xsl:template>
    
    <xsl:template match="gmd:specification/gmd:CI_Citation[../@xlink:href != '']" mode="specinfo" priority="3.0">
        <prov:Entity rdf:about="{iri-to-uri(../@xlink:href)}"/>
    </xsl:template>
    
    <xsl:template name="specinfo">
        <dct:title>
            <xsl:call-template name="xmlLang"/>
            <xsl:value-of select="gmd:title/gco:CharacterString|gmd:title/gmx:Anchor"/>
        </dct:title>
        <xsl:apply-templates select="gmd:date/*/gmd:date/*"/>
    </xsl:template>
    
    <xsl:template match="gmd:dateStamp/*[text() castable as xs:date or text() castable as xs:dateTime]">
        <dct:modified>
            <xsl:call-template name="dateTimeDatatype"/>
        </dct:modified>
    </xsl:template>
    
    <xsl:template name="dateTimeDatatype">
        <xsl:attribute name="rdf:datatype">
            <xsl:choose>
                <xsl:when test="text() castable as xs:date">http://www.w3.org/2001/XMLSchema#date</xsl:when>
                <xsl:otherwise>http://www.w3.org/2001/XMLSchema#dateTime</xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
        <xsl:value-of select="."/>
    </xsl:template>
    
    <xsl:template match="gmd:contact/*" mode="qualifiedAttribution">
        <prov:qualifiedAttribution>
            <prov:Attribution>
                <prov:agent>
                    <xsl:call-template name="vcardOrg"/>
                </prov:agent>
                <dcat:hadRole>
                    <dcat:Role rdf:about="{concat($inspire_md_codelist, 'ResponsiblePartyRole/', encode-for-uri(gmd:role/gmd:CI_RoleCode/@codeListValue))}"/>
                </dcat:hadRole>
            </prov:Attribution>
        </prov:qualifiedAttribution>
    </xsl:template>

    <xsl:template match="gmd:hierarchyLevel[*/@codeListValue = 'dataset' or */@codeListValue = 'series']">
        <dct:type rdf:resource="{concat($inspire_md_codelist, 'ResourceType/', encode-for-uri(*/@codeListValue))}"/>
    </xsl:template>

    <xsl:template match="gmd:hierarchyLevel[*/@codeListValue = 'service']">
        <dct:type rdf:resource="{concat($inspire_md_codelist, 'ResourceType/services')}"/>
        <xsl:apply-templates select="../gmd:identificationInfo/*/srv:serviceType[*/text()]" mode="spatialDataServiceType"/>
    </xsl:template>
    
    <xsl:template match="gmd:hierarchyLevel"/>

    <xsl:template match="srv:serviceType[lower-case(*) = 'wms' or lower-case(*) = 'wmts' or lower-case(*) = 'view' or lower-case(*) = 'ogc:wms' or lower-case(*) = 'ogc:wmts']" mode="spatialDataServiceType">
        <dct:type rdf:resource="{concat($inspire_md_codelist, 'SpatialDataServiceType/view')}"/>
    </xsl:template>
    
    <xsl:template match="srv:serviceType[lower-case(*) = 'wfs' or lower-case(*) = 'download' or lower-case(*) = 'ogc:wfs' or lower-case(*) = 'wcs' or lower-case(*) = 'ogc:wcs']" mode="spatialDataServiceType">
        <dct:type rdf:resource="{concat($inspire_md_codelist, 'SpatialDataServiceType/download')}"/>
    </xsl:template>

    <xsl:template match="srv:serviceType[lower-case(*) = 'csw' or lower-case(*) = 'discovery' or lower-case(*) = 'ogc:csw']" mode="spatialDataServiceType">
        <dct:type rdf:resource="{concat($inspire_md_codelist, 'SpatialDataServiceType/download')}"/>
    </xsl:template>
    
    <xsl:template match="srv:serviceType[lower-case(*) = 'transformation']" mode="spatialDataServiceType">
        <dct:type rdf:resource="{concat($inspire_md_codelist, 'SpatialDataServiceType/transformation')}"/>
    </xsl:template>
    
    <xsl:template match="srv:serviceType[lower-case(*) = 'wps' or lower-case(*) = 'invoke' or lower-case(*) = 'ogc:wps']" mode="spatialDataServiceType">
        <dct:type rdf:resource="{concat($inspire_md_codelist, 'SpatialDataServiceType/transformation')}"/>
    </xsl:template>
    
    <xsl:template match="srv:serviceType" mode="spatialDataServiceType">
        <dct:type rdf:resource="{concat($inspire_md_codelist, 'SpatialDataServiceType/other')}"/>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:citation/*/gmd:title/gco:CharacterString">
        <dct:title>
            <xsl:call-template name="xmlLang"/>
            <xsl:value-of select="."/>
        </dct:title>
        <xsl:apply-templates select="../gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString"/>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/gmd:citation/*/gmd:title/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString">
        <xsl:variable name="localeRef" select="substring-after(@locale, '#')"/>
        <xsl:variable name="locale" select="ancestor::gmd:MD_Metadata/gmd:locale/*[@id = $localeRef]|ancestor::gmi:MI_Metadata/gmd:locale/*[@id = $localeRef]"/>
        <xsl:if test="$locale">
            <xsl:choose>
                <xsl:when test="ancestor::gmd:MD_Metadata/gmd:language/*/gco:CharacterString">
                    <xsl:variable name="languageCode" select="string(ancestor::gmd:MD_Metadata/gmd:language[1]/*/gco:CharacterString)"/>
                    <xsl:if test="not($languageCode = string($locale/gmd:languageCode/*/@codeListValue))">
                        <dct:title>
                            <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                            <xsl:value-of select="."/>
                        </dct:title>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="ancestor::gmd:MD_Metadata/gmd:language/*/@codeListValue">
                    <xsl:variable name="languageCode" select="string(ancestor::gmd:MD_Metadata/gmd:language[1]/*/@codeListValue)"/>
                    <xsl:if test="not($languageCode = string($locale/gmd:languageCode/*/@codeListValue))">
                        <dct:title>
                            <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                            <xsl:value-of select="."/>
                        </dct:title>
                    </xsl:if>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:abstract/gco:CharacterString">
        <dct:description>
            <xsl:call-template name="xmlLang"/>
            <xsl:value-of select="."/>
        </dct:description>
        <xsl:apply-templates select="../gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString"/>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:abstract/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString">
        <xsl:variable name="localeRef" select="substring-after(@locale, '#')"/>
        <xsl:variable name="locale" select="ancestor::gmd:MD_Metadata/gmd:locale/*[@id = $localeRef]|ancestor::gmi:MI_Metadata/gmd:locale/*[@id = $localeRef]"/>
        <xsl:if test="$locale">
            <xsl:choose>
                <xsl:when test="ancestor::gmd:MD_Metadata/gmd:language/*/gco:CharacterString">
                    <xsl:variable name="languageCode" select="string(ancestor::gmd:MD_Metadata/gmd:language[1]/*/gco:CharacterString)"/>
                    <xsl:if test="not($languageCode = string($locale/gmd:languageCode/*/@codeListValue))">
                        <dct:description>
                            <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                            <xsl:value-of select="."/>
                        </dct:description>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="ancestor::gmd:MD_Metadata/gmd:language/*/@codeListValue">
                    <xsl:variable name="languageCode" select="string(ancestor::gmd:MD_Metadata/gmd:language[1]/*/@codeListValue)"/>
                    <xsl:if test="not($languageCode = string($locale/gmd:languageCode/*/@codeListValue))">
                        <dct:description>
                            <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                            <xsl:value-of select="."/>
                        </dct:description>
                    </xsl:if>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/*:extent/*/gmd:geographicElement">
        <dct:spatial>
            <dct:Location>
                <xsl:apply-templates select="../gmd:description/gco:CharacterString"/>
                <xsl:apply-templates select="gmd:EX_GeographicBoundingBox"/>
                <xsl:apply-templates select="gmd:EX_GeographicDescription"/>
            </dct:Location>
        </dct:spatial>
    </xsl:template>
    
    <xsl:template match="gmd:extent/*/gmd:description/gco:CharacterString">
        <rdfs:label>
            <xsl:call-template name="xmlLang"/>
            <xsl:value-of select="text()"/>
        </rdfs:label>
        <xsl:apply-templates select="../gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString"/>
    </xsl:template>

    <xsl:template match="gmd:extent/*/gmd:description/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString">
        <xsl:variable name="localeRef" select="substring-after(@locale, '#')"/>
        <xsl:variable name="locale" select="ancestor::gmd:MD_Metadata/gmd:locale/*[@id = $localeRef]|ancestor::gmi:MI_Metadata/gmd:locale/*[@id = $localeRef]"/>
        <xsl:if test="$locale">
            <rdfs:label>
                <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                <xsl:value-of select="."/>
            </rdfs:label>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="gmd:geographicElement/gmd:EX_GeographicBoundingBox">
        <dcat:bbox rdf:datatype="http://www.opengis.net/ont/geosparql#wktLiteral">
            <xsl:text disable-output-escaping="yes">&lt;![CDATA[POLYGON((</xsl:text>
            <xsl:value-of select="concat(gmd:westBoundLongitude/*, ' ', gmd:northBoundLatitude/*, ',', gmd:eastBoundLongitude/*, ' ', gmd:northBoundLatitude/*, ',', gmd:eastBoundLongitude/*, ' ', gmd:southBoundLatitude/*,',')"/>
            <xsl:value-of select="concat(gmd:westBoundLongitude/*, ' ', gmd:southBoundLatitude/*, ',', gmd:westBoundLongitude/*, ' ', gmd:northBoundLatitude/*)"/>
            <xsl:text disable-output-escaping="yes">))]]&gt;</xsl:text>
        </dcat:bbox>
    </xsl:template>
    
    <xsl:template match="gmd:geographicElement/gmd:EX_GeographicDescription[gmd:geographicIdentifier/gmd:MD_Identifier/gmd:code]">
        <rdfs:seeAlso>
            <skos:Concept>
                <xsl:apply-templates select="gmd:geographicIdentifier/gmd:MD_Identifier/gmd:code/gco:CharacterString|gmd:geographicIdentifier/gmd:MD_Identifier/gmd:code/gmx:Anchor"/>
                <xsl:apply-templates select="gmd:geographicIdentifier/gmd:MD_Identifier/gmd:authority/gmd:CI_Citation"/>
            </skos:Concept>
        </rdfs:seeAlso>
    </xsl:template>
    
    <xsl:template match="gmd:geographicIdentifier/gmd:MD_Identifier/gmd:code/gco:CharacterString">
        <skos:prefLabel>
            <xsl:call-template name="xmlLang"/>
            <xsl:value-of select="text()"/>
        </skos:prefLabel>
    </xsl:template>
    
    <xsl:template match="gmd:geographicIdentifier/gmd:MD_Identifier/gmd:code/gmx:Anchor">
        <xsl:attribute name="rdf:about"><xsl:value-of select="encode-for-uri(@xlink:href)"/></xsl:attribute>
        <xsl:if test="text()">
            <skos:prefLabel>
                <xsl:call-template name="xmlLang"/>
                <xsl:value-of select="text()"/>
            </skos:prefLabel>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="gmd:authority/gmd:CI_Citation">
        <skos:inScheme>
            <skos:ConceptScheme>
                <dct:title>
                    <xsl:call-template name="xmlLang"/>
                    <xsl:value-of select="gmd:title/gco:CharacterString"/>
                </dct:title>
                <xsl:apply-templates select="gmd:title/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString"/>
                <xsl:apply-templates select="gmd:date/*/gmd:date/*"/>
            </skos:ConceptScheme>
        </skos:inScheme>
    </xsl:template>
    
    <xsl:template match="gmd:authority/gmd:CI_Citation/gmd:title/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString">
        <xsl:variable name="localeRef" select="substring-after(@locale, '#')"/>
        <xsl:variable name="locale" select="ancestor::gmd:MD_Metadata/gmd:locale/*[@id = $localeRef]|ancestor::gmi:MI_Metadata/gmd:locale/*[@id = $localeRef]"/>
        <xsl:if test="$locale">
            <dct:title>
                <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                <xsl:value-of select="."/>
            </dct:title>
        </xsl:if>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/*:extent/*/gmd:temporalElement/*/gmd:extent/gml:TimePeriod">
        <dct:temporal>
            <dct:PeriodOfTime>
                <xsl:apply-templates select="gml:beginPosition"/>
                <xsl:apply-templates select="gml:endPosition"/>
            </dct:PeriodOfTime>
        </dct:temporal>
    </xsl:template>
    
    <xsl:template match="gml:beginPosition[text() castable as xs:date or text() castable as xs:dateTime]">
        <dcat:startDate>
            <xsl:call-template name="dateTimeDatatype"/>
        </dcat:startDate>
    </xsl:template>
    
    <xsl:template match="gml:endPosition[text() castable as xs:date or text() castable as xs:dateTime]">
        <dcat:endDate>
            <xsl:call-template name="dateTimeDatatype"/>
        </dcat:endDate>
    </xsl:template>

    <xsl:template match="gmd:date/gmd:CI_Date[gmd:dateType/*/@codeListValue = 'publication']/gmd:date/*[text() castable as xs:date or text() castable as xs:dateTime]">
        <dct:issued>
            <xsl:call-template name="dateTimeDatatype"/>
        </dct:issued>
    </xsl:template>
    
    <xsl:template match="gmd:date/gmd:CI_Date[gmd:dateType/*/@codeListValue = 'revision']/gmd:date/*[text() castable as xs:date or text() castable as xs:dateTime]">
        <dct:modified>
            <xsl:call-template name="dateTimeDatatype"/>
        </dct:modified>
    </xsl:template>
    
    <xsl:template match="gmd:date/gmd:CI_Date[gmd:dateType/*/@codeListValue = 'creation']/gmd:date/*[text() castable as xs:date or text() castable as xs:dateTime]">
        <dct:created>
            <xsl:call-template name="dateTimeDatatype"/>
        </dct:created>
    </xsl:template>
    
    <!-- GeoDCAT-AP 2 maps only pointOfContact
    <xsl:template match="gmd:identificationInfo/*/gmd:citation/*/gmd:citedResponsibleParty/*[gmd:role/*/@codeListValue='publisher' and gmd:organisationName]">
        <dct:publisher>
            <xsl:call-template name="foafOrg"/>
        </dct:publisher>
        <xsl:if test="$extended">
            <xsl:call-template name="qualifiedAttribution"/>
        </xsl:if>
    </xsl:template>
    -->
    
    <xsl:template match="gmd:identificationInfo/*/gmd:pointOfContact/*[gmd:role/gmd:CI_RoleCode/@codeListValue='pointOfContact']|gmd:contact/*">
        <dcat:contactPoint>
            <xsl:call-template name="vcardOrg"/>
        </dcat:contactPoint>
        <xsl:if test="$extended">
            <xsl:call-template name="qualifiedAttribution"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:pointOfContact/*[gmd:role/gmd:CI_RoleCode/@codeListValue='publisher']">
        <dct:publisher>
            <xsl:call-template name="foafOrg"/>
        </dct:publisher>
        <xsl:if test="$extended">
            <xsl:call-template name="qualifiedAttribution"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/gmd:pointOfContact/*[gmd:role/gmd:CI_RoleCode/@codeListValue='author']">
        <dct:creator>
            <xsl:call-template name="foafOrg"/>
        </dct:creator>
        <xsl:if test="$extended">
            <xsl:call-template name="qualifiedAttribution"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/gmd:pointOfContact/*[gmd:role/gmd:CI_RoleCode/@codeListValue='originator']">
        <xsl:if test="$extended">
            <geodcatap:originator>
                <xsl:call-template name="foafOrg"/>
            </geodcatap:originator>
            <xsl:call-template name="qualifiedAttribution"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/gmd:pointOfContact/*[gmd:role/gmd:CI_RoleCode/@codeListValue='owner']">
        <xsl:if test="$extended">
            <dct:rightsHolder>
                <xsl:call-template name="foafOrg"/>
            </dct:rightsHolder>
            <xsl:call-template name="qualifiedAttribution"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/gmd:pointOfContact/*[gmd:role/gmd:CI_RoleCode/@codeListValue='custodian']">
        <xsl:if test="$extended">
            <geodcatap:custodian>
                <xsl:call-template name="foafOrg"/>
            </geodcatap:custodian>
            <xsl:call-template name="qualifiedAttribution"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/gmd:pointOfContact/*[gmd:role/gmd:CI_RoleCode/@codeListValue='principalInvestigator']">
        <xsl:if test="$extended">
            <geodcatap:principalInvestigator>
                <xsl:call-template name="foafOrg"/>
            </geodcatap:principalInvestigator>
            <xsl:call-template name="qualifiedAttribution"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/gmd:pointOfContact/*[gmd:role/gmd:CI_RoleCode/@codeListValue='user']">
        <xsl:if test="$extended">
            <geodcatap:user>
                <xsl:call-template name="foafOrg"/>
            </geodcatap:user>
            <xsl:call-template name="qualifiedAttribution"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/gmd:pointOfContact/*[gmd:role/gmd:CI_RoleCode/@codeListValue='distributor']">
        <xsl:if test="$extended">
            <geodcatap:distributor>
                <xsl:call-template name="foafOrg"/>
            </geodcatap:distributor>
            <xsl:call-template name="qualifiedAttribution"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/gmd:pointOfContact/*[gmd:role/gmd:CI_RoleCode/@codeListValue='processor']">
        <xsl:if test="$extended">
            <geodcatap:processor>
                <xsl:call-template name="foafOrg"/>
            </geodcatap:processor>
            <xsl:call-template name="qualifiedAttribution"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="qualifiedAttribution">
        <prov:qualifiedAttribution>
            <prov:Attribution>
                <prov:agent>
                    <xsl:call-template name="foafOrg"/>
                </prov:agent>
                <dcat:hadRole>
                    <dcat:Role rdf:about="{concat($inspire_md_codelist, 'ResponsiblePartyRole/', encode-for-uri(gmd:role/gmd:CI_RoleCode/@codeListValue))}"/>
                </dcat:hadRole>
            </prov:Attribution>
        </prov:qualifiedAttribution>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/gmd:pointOfContact/*" priority="0.25"/>
    
    <xsl:template name="foafOrg">
        <rdf:Description>
            <xsl:variable name="orgLink" select="string(gmd:organisationName/gmx:Anchor/@xlink:href)"/>
            <xsl:variable name="indLink" select="string(gmd:individualName/gmx:Anchor/@xlink:href)"/>
            <xsl:choose>
                <xsl:when test="$indLink != ''">
                    <xsl:attribute name="rdf:about"><xsl:value-of select="iri-to-uri($indLink)"/></xsl:attribute>
                </xsl:when>
                <xsl:when test="$orgLink != ''">
                    <xsl:attribute name="rdf:about"><xsl:value-of select="iri-to-uri($orgLink)"/></xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <xsl:variable name="orgName" select="string(gmd:organisationName/gco:CharacterString)"/>
            <xsl:variable name="indName" select="string(gmd:individualName[1]/gco:CharacterString)"/>
            <xsl:choose>
                <xsl:when test="$indName != ''">
                    <rdf:type rdf:resource="http://xmlns.com/foaf/0.1/Person"/>
                </xsl:when>
                <xsl:when test="$orgName != ''">
                    <rdf:type rdf:resource="http://xmlns.com/foaf/0.1/Organization"/>
                </xsl:when>
                <xsl:otherwise>
                    <rdf:type rdf:resource="http://xmlns.com/foaf/0.1/Agent"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="gmd:organisationName[not(../gmd:individualName/gco:CharacterString/text())]/gco:CharacterString[text()]"/>
            <xsl:apply-templates select="gmd:individualName/gco:CharacterString[text()]"/>
            <xsl:apply-templates select="gmd:contactInfo/*/gmd:address/*/gmd:electronicMailAddress/gco:CharacterString[not(contains(text, ';') or contains(text, ',') or contains(text, ' '))]"/>
            <xsl:apply-templates select="gmd:contactInfo/*/gmd:phone/*/gmd:voice/*[text()]"/>
            <xsl:apply-templates select="gmd:contactInfo/*/gmd:onlineResource/*/gmd:linkage/*[text()]"/>
            <xsl:apply-templates select="gmd:contactInfo/*/gmd:address/*"/>
        </rdf:Description>
    </xsl:template>
    
    <xsl:template match="gmd:organisationName/gco:CharacterString">
        <foaf:name><xsl:value-of select="."/></foaf:name>
    </xsl:template>
    
    <xsl:template match="gmd:individualName/gco:CharacterString">
        <foaf:name><xsl:value-of select="."/></foaf:name>
        <xsl:apply-templates select="../../gmd:organisationName/gco:CharacterString[text()]" mode="memberOf"/>
    </xsl:template>
    
    <xsl:template match="gmd:organisationName/gco:CharacterString" mode="memberOf">
        <org:memberOf>
            <foaf:Organization>
                <xsl:apply-templates select="."/>
            </foaf:Organization>
        </org:memberOf>
    </xsl:template>
    
    <xsl:template match="gmd:contactInfo/*/gmd:address/*/gmd:electronicMailAddress/*">
        <foaf:mbox rdf:resource="{concat('mailto:', text())}"/>
    </xsl:template>
    
    <xsl:template match="gmd:contactInfo/*/gmd:phone/*/gmd:voice/*">
        <foaf:phone rdf:resource="{concat('tel:+', encode-for-uri(.))}"/>
    </xsl:template>
    
    <xsl:template match="gmd:contactInfo/*/gmd:onlineResource/*/gmd:linkage/*">
        <foaf:homepage rdf:resource="{iri-to-uri(text())}" />
    </xsl:template>
    
    <xsl:template match="gmd:contactInfo/*/gmd:address/*">
        <locn:address>
            <locn:Address>
                <xsl:apply-templates select="gmd:deliveryPoint/*[text()]"/>
                <xsl:apply-templates select="gmd:city/*[text()]"/>
                <xsl:apply-templates select="gmd:postalCode/*[text()]"/>
                <xsl:apply-templates select="gmd:administrativeArea/*[text()]"/>
                <xsl:apply-templates select="gmd:country/*[text()]"/>
            </locn:Address>
        </locn:address>
    </xsl:template>
    
    <xsl:template match="gmd:deliveryPoint/*">
        <locn:thoroughfare><xsl:value-of select="."/></locn:thoroughfare>
    </xsl:template>
    
    <xsl:template match="gmd:city/*">
        <locn:postName><xsl:value-of select="."/></locn:postName>
    </xsl:template>
    
    <xsl:template match="gmd:postalCode/*">
        <locn:postCode><xsl:value-of select="."/></locn:postCode>
    </xsl:template>
    
    <xsl:template match="gmd:administrativeArea/*">
        <locn:adminUnitL2><xsl:value-of select="."/></locn:adminUnitL2>
    </xsl:template>
    
    <xsl:template match="gmd:country/*">
        <locn:adminUnitL1><xsl:value-of select="."/></locn:adminUnitL1>
    </xsl:template>

    <xsl:template name="vcardOrg">
        <rdf:Description>
            <xsl:variable name="orgLink" select="string(gmd:organisationName/gmx:Anchor/@xlink:href)"/>
            <xsl:variable name="indLink" select="string(gmd:individualName/gmx:Anchor/@xlink:href)"/>
            <xsl:choose>
                <xsl:when test="$indLink != ''">
                    <xsl:attribute name="rdf:about"><xsl:value-of select="iri-to-uri($indLink)"/></xsl:attribute>
                </xsl:when>
                <xsl:when test="$orgLink != ''">
                    <xsl:attribute name="rdf:about"><xsl:value-of select="iri-to-uri($orgLink)"/></xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <xsl:variable name="orgName" select="string(gmd:organisationName/gco:CharacterString)"/>
            <xsl:variable name="indName" select="string(gmd:individualName/gco:CharacterString)"/>
            <xsl:choose>
                <xsl:when test="$indName != ''">
                    <rdf:type rdf:resource="http://www.w3.org/2006/vcard/ns#Individual"/>
                </xsl:when>
                <xsl:when test="$orgName != ''">
                    <rdf:type rdf:resource="http://www.w3.org/2006/vcard/ns#Organization"/>
                </xsl:when>
                <xsl:otherwise>
                    <rdf:type rdf:resource="http://www.w3.org/2006/vcard/ns#Kind"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:apply-templates select="gmd:organisationName/gco:CharacterString[text()]" mode="vcard"/>
            <xsl:apply-templates select="gmd:individualName/gco:CharacterString[text()]" mode="vcard"/>
            <xsl:apply-templates select="gmd:contactInfo/*/gmd:address/*/gmd:electronicMailAddress/gco:CharacterString[not(contains(text, ';') or contains(text, ',') or contains(text, ' '))]" mode="vcard"/>
            <xsl:apply-templates select="gmd:contactInfo/*/gmd:phone/*/gmd:voice/*[text()]"  mode="vcard"/>
            <xsl:apply-templates select="gmd:contactInfo/*/gmd:onlineResource/*/gmd:linkage/*[text()]" mode="vcard"/>
            <xsl:apply-templates select="gmd:contactInfo/*/gmd:address/*" mode="vcard"/>
        </rdf:Description>
    </xsl:template>

    <xsl:template match="gmd:organisationName/gco:CharacterString[../../gmd:individualName/gco:CharacterString]" mode="vcard">
        <vcard:organization-name><xsl:value-of select="."/></vcard:organization-name>
    </xsl:template>
    
    <xsl:template match="gmd:individualName/gco:CharacterString | gmd:organisationName/gco:CharacterString[not(../../gmd:individualName/gco:CharacterString)]" mode="vcard">
        <vcard:fn><xsl:value-of select="."/></vcard:fn>
    </xsl:template>
    
    <xsl:template match="gmd:electronicMailAddress/*" mode="vcard">
        <vcard:hasEmail rdf:resource="{concat('mailto:', .)}"/>
    </xsl:template>
    
    <xsl:template match="gmd:contactInfo/*/gmd:phone/*/gmd:voice/*" mode="vcard">
        <vcard:hasTelephone rdf:resource="tel:+{translate(translate(translate(translate(translate(normalize-space(.),' ',''),'(',''),')',''),'+',''),'.','')}"/>
    </xsl:template>
    
    <xsl:template match="gmd:contactInfo/*/gmd:onlineResource/*/gmd:linkage/*" mode="vcard">
        <vcard:hasURL rdf:resource="{iri-to-uri(.)}"/>
    </xsl:template>
    
    <xsl:template match="gmd:contactInfo/*/gmd:address/*" mode="vcard">
        <vcard:hasAddress>
            <vcard:Address>
                <xsl:apply-templates select="gmd:deliveryPoint/*[text()]" mode="vcard"/>
                <xsl:apply-templates select="gmd:city/*[text()]" mode="vcard"/>
                <xsl:apply-templates select="gmd:postalCode/*[text()]" mode="vcard"/>
                <xsl:apply-templates select="gmd:administrativeArea/*[text()]" mode="vcard"/>
                <xsl:apply-templates select="gmd:country/*[text()]" mode="vcard"/>
            </vcard:Address>
        </vcard:hasAddress>
    </xsl:template>

    <xsl:template match="gmd:deliveryPoint/*" mode="vcard">
        <vcard:street-address><xsl:value-of select="."/></vcard:street-address>
    </xsl:template>

    <xsl:template match="gmd:city/*" mode="vcard">
        <vcard:locality><xsl:value-of select="."/></vcard:locality>
    </xsl:template>
    
    <xsl:template match="gmd:postalCode/*" mode="vcard">
        <vcard:postal-code><xsl:value-of select="."/></vcard:postal-code>
    </xsl:template>
    
    <xsl:template match="gmd:administrativeArea/*" mode="vcard">
        <vcard:region><xsl:value-of select="."/></vcard:region>
    </xsl:template>
    
    <xsl:template match="gmd:country/*" mode="vcard">
        <vcard:country-name><xsl:value-of select="."/></vcard:country-name>
    </xsl:template>
    
    <xsl:template name="serviceDistribution">
        <xsl:variable name="accessUrl">
            <xsl:variable name="getCapsUrl" select="gmd:identificationInfo[1]/*/srv:containsOperations/*[srv:operationName/* = 'GetCapabilities']/srv:connectPoint/*/gmd:linkage/*"/>
            <xsl:variable name="getCapsUrlProto" select="gmd:distributionInfo/*/gmd:transferOptions/*/gmd:onLine/*[gmd:protocol/* = 'HTTP:OGC:WMS']/gmd:linkage/*[text()] | gmd:distributionInfo/*/gmd:distributor/*/gmd:distributorTransferOptions/*/gmd:onLine/*[gmd:protocol/* = 'HTTP:OGC:WMS']/gmd:linkage/*[text()]"/>
            <xsl:variable name="getCapsUrlParam" select="gmd:distributionInfo/*/gmd:transferOptions/*/gmd:onLine/*/gmd:linkage/*[contains(., 'GetCapabilities')] | gmd:distributionInfo/*/gmd:distributor/*/gmd:distributorTransferOptions/*/gmd:onLine/*/gmd:linkage/*[contains(., 'GetCapabilities')]"/>
            <xsl:variable name="distrUrl" select="gmd:distributionInfo/*/gmd:transferOptions/*/gmd:onLine/*/gmd:linkage/*[text()] | gmd:distributionInfo/*/gmd:distributor/*/gmd:distributorTransferOptions/*/gmd:onLine/*/gmd:linkage/*[text()]"/>
            <xsl:choose>
                <xsl:when test="$getCapsUrl">
                    <xsl:value-of select="$getCapsUrl"/>
                </xsl:when>
                <xsl:when test="count($getCapsUrlProto) &gt; 0">
                    <xsl:value-of select="$getCapsUrlProto[1]"/>
                </xsl:when>
                <xsl:when test="count($getCapsUrlParam) &gt; 0">
                    <xsl:value-of select="$getCapsUrlParam[1]"/>
                </xsl:when>
                <xsl:when test="count($distrUrl) &gt; 0">
                    <xsl:value-of select="$distrUrl[1]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:if test="$accessUrl != ''">
            <dcat:distribution>
                <dcat:Distribution>
                    <xsl:apply-templates select="gmd:identificationInfo[1]/*/srv:serviceType/*[text()]" mode="title"/>
                    <dcat:accessURL rdf:resource="{iri-to-uri($accessUrl)}"/>
                    <xsl:apply-templates select="gmd:identificationInfo[1]/*/srv:serviceType[*/text()]">
                        <xsl:with-param name="accessUrl" select="$accessUrl"/>
                    </xsl:apply-templates>
                    <xsl:call-template name="constraints"/>
                    <xsl:if test="$extended">
                        <xsl:apply-templates select="gmd:identificationInfo/*/gmd:characterSet/*/@codeListValue|gmd:identificationInfo/*/gmd:characterSet/*/@codeListValue"/>
                    </xsl:if>
                </dcat:Distribution>
            </dcat:distribution>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="srv:serviceType/*" mode="title">
        <dct:title xml:lang="en">
            <xsl:value-of select="text()"/>
        </dct:title>
    </xsl:template>
    
    <xsl:template name="formatWithLabel">
        <xsl:param name="label"/>
        <dct:format rdf:parseType="Resource">
            <rdf:type rdf:resource="http://purl.org/dc/terms/MediaType"/>
            <rdfs:label><xsl:value-of select="$label"/></rdfs:label>
        </dct:format>
    </xsl:template>

    <xsl:template match="srv:serviceType[lower-case(*) = 'wms' or lower-case(*) = 'view' or lower-case(*) = 'ogc:wms']">
        <xsl:param name="accessUrl"/>
        <dct:format>
            <dct:MediaType rdf:about="http://publications.europa.eu/resource/authority/file-type/WMS_SRVC"/>
        </dct:format>
        <xsl:call-template name="accessServiceForService">
            <xsl:with-param name="title" select="'WMS'"/>
            <xsl:with-param name="accessUrl" select="$accessUrl"/>
            <xsl:with-param name="conformsTo" select="'http://www.opengeospatial.org/standards/wms'"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="srv:serviceType[lower-case(*) = 'wmts' or lower-case(*) = 'ogc:wmts']">
        <xsl:param name="accessUrl"/>
        <xsl:call-template name="formatWithLabel">
            <xsl:with-param name="label" select="'WMTS'"/>
        </xsl:call-template>
        <xsl:call-template name="accessServiceForService">
            <xsl:with-param name="title" select="'WMTS'"/>
            <xsl:with-param name="accessUrl" select="$accessUrl"/>
            <xsl:with-param name="conformsTo" select="'http://www.opengeospatial.org/standards/wmts'"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="srv:serviceType[lower-case(*) = 'wfs' or lower-case(*) = 'ogc:wfs']">
        <xsl:param name="accessUrl"/>
        <dct:format>
            <dct:MediaType rdf:about="http://publications.europa.eu/resource/authority/file-type/WFS_SRVC"/>
        </dct:format>
        <xsl:call-template name="accessServiceForService">
            <xsl:with-param name="title" select="'WFS'"/>
            <xsl:with-param name="accessUrl" select="$accessUrl"/>
            <xsl:with-param name="conformsTo" select="'http://www.opengeospatial.org/standards/wfs'"/>
        </xsl:call-template>
    </xsl:template>

    <!-- download could also be WCS? -->
    <xsl:template match="srv:serviceType[lower-case(*) = 'download']">
        <xsl:param name="accessUrl"/>
        <xsl:variable name="serviceTypeVersion" select="../srv:serviceTypeVersion"/>
        <xsl:choose>
            <xsl:when test="$serviceTypeVersion[contains(*,'ATOM')]">
                <dct:format>
                    <dct:MediaType rdf:about="http://publications.europa.eu/resource/authority/file-type/ATOM"/>
                </dct:format>
                <xsl:call-template name="accessServiceForService">
                    <xsl:with-param name="title" select="'ATOM'"/>
                    <xsl:with-param name="accessUrl" select="$accessUrl"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="$serviceTypeVersion[contains(*,'WFS')]">
                <dct:format>
                    <dct:MediaType rdf:about="http://publications.europa.eu/resource/authority/file-type/WFS_SRVC"/>
                </dct:format>
                <xsl:call-template name="accessServiceForService">
                    <xsl:with-param name="title" select="'WFS'"/>
                    <xsl:with-param name="accessUrl" select="$accessUrl"/>
                    <xsl:with-param name="conformsTo" select="'http://www.opengeospatial.org/standards/wfs'"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <dct:format rdf:parseType="Resource">
                    <rdf:type rdf:resource="http://purl.org/dc/terms/MediaType"/>
                    <rdfs:label>INSPIRE Download Service</rdfs:label>
                    <xsl:call-template name="accessServiceForService">
                        <xsl:with-param name="title" select="'Download'"/>
                        <xsl:with-param name="accessUrl" select="$accessUrl"/>
                    </xsl:call-template>
                </dct:format>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="srv:serviceType[lower-case(*) = 'wcs' or lower-case(*) = 'ogc:wcs']">
        <xsl:param name="accessUrl"/>
        <xsl:call-template name="formatWithLabel">
            <xsl:with-param name="label" select="'WCS'"/>
        </xsl:call-template>
        <xsl:call-template name="accessServiceForService">
            <xsl:with-param name="title" select="'WCS'"/>
            <xsl:with-param name="accessUrl" select="$accessUrl"/>
            <xsl:with-param name="conformsTo" select="'http://www.opengeospatial.org/standards/wcs'"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="srv:serviceType">
        <xsl:param name="accessUrl"/>
        <xsl:call-template name="formatWithLabel">
            <xsl:with-param name="label" select="*"/>
        </xsl:call-template>
        <xsl:call-template name="accessServiceForService">
            <xsl:with-param name="title" select="'WFS'"/>
            <xsl:with-param name="accessUrl" select="$accessUrl"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template name="accessServiceForService">
        <xsl:param name="title" as="xs:string"/>
        <xsl:param name="accessUrl" as="xs:string"/>
        <xsl:param name="conformsTo" as="xs:string" select="''"/>
        
        <dcat:accessService rdf:parseType="Resource">
            <rdf:type rdf:resource="http://www.w3.org/ns/dcat#DataService"/>
            <dct:title xml:lang="en">
                <xsl:value-of select="$title"/>
            </dct:title>
            <dcat:endpointURL rdf:resource="{if (contains($accessUrl, '?')) then substring-before($accessUrl, '?') else $accessUrl}"/>
            <dcat:endpointDescription rdf:resource="{$accessUrl}"/>
            <xsl:if test="$extended and $conformsTo != ''">
                <dct:conformsTo>
                    <dct:Standard rdf:about="{$conformsTo}"/>
                </dct:conformsTo>
            </xsl:if>
            <!-- do not add the dcat:servesDataset to the service distribution, since this is already part of dataset metadata
            <xsl:apply-templates select="../srv:operatesOn/@uuidref | ../srv:operatesOn[not(@uuidref)]/@href"/>
            -->
        </dcat:accessService>        
    </xsl:template>

    <xsl:template name="dataDistribution">
        <xsl:variable name="distributorLinks" select="gmd:distributionInfo/*/gmd:distributor/*/gmd:distributorTransferOptions/*/gmd:onLine/*[gmd:linkage/*[text()]]"/>
        <xsl:variable name="distributionLinks" select="gmd:distributionInfo/*/gmd:transferOptions/*/gmd:onLine/*[gmd:linkage/*[text()]]"/>
        <xsl:for-each select="$distributorLinks">
            <xsl:variable name="link" select="gmd:linkage/*/text()"/>
            <xsl:if test="not($distributionLinks/gmd:linkage/*[text() = $link])">
                <xsl:apply-templates select="."/>
            </xsl:if>
        </xsl:for-each>
        <xsl:apply-templates select="$distributionLinks"/>
    </xsl:template>

    <xsl:template match="gmd:distributorTransferOptions/*/gmd:onLine/* | gmd:transferOptions/*/gmd:onLine/*">
        <xsl:variable name="linkage" select="gmd:linkage/gmd:URL"/>
        <xsl:variable name="function" select="gmd:function/*/@codeListValue"/>
        <xsl:variable name="httpQuery" select="lower-case(substring-after($linkage, '?'))"/>
        <xsl:variable name="isServiceLinkage" select="contains($httpQuery, 'request=getcapabilities')"/>
        <xsl:choose>
            <xsl:when test="$isServiceLinkage or $function = 'download' or $function = 'offlineAccess' or $function = 'order'">
                <dcat:distribution>
                    <dcat:Distribution>
                        <xsl:apply-templates select="gmd:name/gco:CharacterString[text()]"/>
                        <xsl:apply-templates select="gmd:description/gco:CharacterString[text()]"/>
                        <xsl:apply-templates select="gmd:linkage/*"/>
                        <xsl:if test="$isServiceLinkage">
                            <dcat:accessService rdf:parseType="Resource">
                                <rdf:type rdf:resource="http://www.w3.org/ns/dcat#DataService"/>
                                <dct:title>
                                    <xsl:call-template name="xmlLang"/>
                                    <xsl:variable name="name" select="gmd:name/gco:CharacterString[text()]"/>
                                    <xsl:variable name="description" select="gmd:description/gco:CharacterString[text()]"/>
                                    <xsl:value-of select="if ($name) then $name else (if ($description) then $description else 'N/A')"/>
                                </dct:title>
                                <dcat:endpointURL rdf:resource="{if (contains($linkage, '?')) then substring-before($linkage, '?') else $linkage}"/>
                                <dcat:endpointDescription rdf:resource="{$linkage}"/>
                                <xsl:if test="$extended">
                                    <xsl:variable name="conformsTo">
                                        <xsl:call-template name="conformsToService">
                                            <xsl:with-param name="linkage" select="$linkage"/>
                                        </xsl:call-template>
                                    </xsl:variable>
                                    <xsl:variable name="protocol-url" select="gmd:protocol/gmx:Anchor/@xlink:href"/>
                                    <xsl:choose>
                                        <xsl:when test="$protocol-url != ''">
                                            <dct:conformsTo>
                                                <dct:Standard rdf:about="{$protocol-url}"/>
                                            </dct:conformsTo>
                                        </xsl:when>
                                        <xsl:when test="$conformsTo != ''">
                                            <dct:conformsTo>
                                                <dct:Standard rdf:about="{$conformsTo}"/>
                                            </dct:conformsTo>
                                        </xsl:when>
                                    </xsl:choose>
                                </xsl:if>
                            </dcat:accessService>
                        </xsl:if>
                        <xsl:variable name="distributorFormat" select="../../../../gmd:distributorFormat/*/gmd:name/gco:CharacterString[text()]"/>
                        <xsl:choose>
                            <xsl:when test="$isServiceLinkage and contains($httpQuery, 'service=wms')">
                                <dct:format>
                                    <dct:MediaType rdf:about="http://publications.europa.eu/resource/authority/file-type/WMS_SRVC"/>
                                </dct:format>
                            </xsl:when>
                            <xsl:when test="$isServiceLinkage and contains($httpQuery, 'service=wfs')">
                                <dct:format>
                                    <dct:MediaType rdf:about="http://publications.europa.eu/resource/authority/file-type/WFS_SRVC"/>
                                </dct:format>
                            </xsl:when>
                            <xsl:when test="$distributorFormat">
                                <xsl:apply-templates select="$distributorFormat"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:apply-templates select="ancestor::gmd:distributionInfo/*/gmd:distributionFormat[1]/*/gmd:name/gco:CharacterString[text()]"/>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:call-template name="constraints"/>
                        <xsl:if test="$extended">
                            <xsl:apply-templates select="ancestor::gmd:MD_Metadata/gmd:identificationInfo/*/gmd:characterSet/*/@codeListValue|ancestor::gmi:MI_Metadata/gmd:identificationInfo/*/gmd:characterSet/*/@codeListValue"/>
                            <xsl:apply-templates select="ancestor::gmd:MD_Metadata/gmd:identificationInfo/*/gmd:spatialRepresentationType/*/@codeListValue|ancestor::gmi:MI_Metadata/gmd:identificationInfo/*/gmd:spatialRepresentationType/*/@codeListValue"/>
                        </xsl:if>
                    </dcat:Distribution>
                </dcat:distribution>
            </xsl:when>
            <xsl:when test="$function = 'information' or $function = 'search'">
                <foaf:page>
                    <xsl:call-template name="linkedDocument"/>
                </foaf:page>
            </xsl:when>
            <xsl:when test="not($function)">
                <dcat:landingPage>
                    <xsl:call-template name="linkedDocument"/>
                </dcat:landingPage>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="linkedDocument">
        <foaf:Document rdf:about="{iri-to-uri(gmd:linkage/gmd:URL)}">
            <xsl:apply-templates select="gmd:name/gco:CharacterString[text()]"/>
            <xsl:apply-templates select="gmd:description/gco:CharacterString[text()]"/>
        </foaf:Document>
    </xsl:template>
    
    <xsl:template name="conformsToService">
        <xsl:param name="linkage" as="xs:string"/>
        <xsl:choose>
            <xsl:when test="contains($linkage, 'csw')">
                <xsl:text>http://www.opengeospatial.org/standards/cat</xsl:text>
            </xsl:when>
            <xsl:when test="contains($linkage, 'sos')">
                <xsl:text>http://www.opengeospatial.org/standards/sos</xsl:text>
            </xsl:when>
            <xsl:when test="contains($linkage, 'sps')">
                <xsl:text>http://www.opengeospatial.org/standards/sps</xsl:text>
            </xsl:when>
            <xsl:when test="contains($linkage, 'wcs')">
                <xsl:text>http://www.opengeospatial.org/standards/wcs</xsl:text>
            </xsl:when>
            <xsl:when test="contains($linkage, 'wfs')">
                <xsl:text>http://www.opengeospatial.org/standards/wfs</xsl:text>
            </xsl:when>
            <xsl:when test="contains($linkage, 'wms')">
                <xsl:text>http://www.opengeospatial.org/standards/wms</xsl:text>
            </xsl:when>
            <xsl:when test="contains($linkage, 'wmts')">
                <xsl:text>http://www.opengeospatial.org/standards/wmts</xsl:text>
            </xsl:when>
            <xsl:when test="contains($linkage, 'wps')">
                <xsl:text>http://www.opengeospatial.org/standards/wps</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="constraints">
        <xsl:apply-templates select="
            ancestor-or-self::gmd:MD_Metadata/gmd:identificationInfo/*/gmd:resourceConstraints/*[gmd:accessConstraints]/gmd:otherConstraints |
            ancestor-or-self::gmd:MD_Metadata/gmd:identificationInfo/*/gmd:resourceConstraints/*[gmd:useConstraints]/gmd:otherConstraints |
            ancestor-or-self::gmi:MI_Metadata/gmd:identificationInfo/*/gmd:resourceConstraints/*[gmd:accessConstraints]/gmd:otherConstraints |
            ancestor-or-self::gmi:MI_Metadata/gmd:identificationInfo/*/gmd:resourceConstraints/*[gmd:useConstraints]/gmd:otherConstraints"/>
    </xsl:template>
    
    <xsl:template match="gmd:onLine/*/gmd:name/*">
        <dct:title>
            <xsl:call-template name="xmlLang"/>
            <xsl:value-of select="."/>
        </dct:title>
    </xsl:template>
    
    <xsl:template match="gmd:onLine/*/gmd:description/gco:CharacterString">
        <dct:description>
            <xsl:call-template name="xmlLang"/>
            <xsl:value-of select="."/>
        </dct:description>
        <xsl:apply-templates select="../gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString"/>
    </xsl:template>
    
    <xsl:template match="gmd:onLine/*/gmd:description/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString">
        <xsl:variable name="localeRef" select="substring-after(@locale, '#')"/>
        <xsl:variable name="locale" select="ancestor::gmd:MD_Metadata/gmd:locale/*[@id = $localeRef]|ancestor::gmi:MI_Metadata/gmd:locale/*[@id = $localeRef]"/>
        <xsl:if test="$locale">
            <xsl:choose>
                <xsl:when test="ancestor::gmd:MD_Metadata/gmd:language/*/gco:CharacterString">
                    <xsl:variable name="languageCode" select="string(ancestor::gmd:MD_Metadata/gmd:language[1]/*/gco:CharacterString)"/>
                    <xsl:if test="not($languageCode = string($locale/gmd:languageCode/*/@codeListValue))">
                        <dct:description>
                            <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                            <xsl:value-of select="."/>
                        </dct:description>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="ancestor::gmd:MD_Metadata/gmd:language/*/@codeListValue">
                    <xsl:variable name="languageCode" select="string(ancestor::gmd:MD_Metadata/gmd:language[1]/*/@codeListValue)"/>
                    <xsl:if test="not($languageCode = string($locale/gmd:languageCode/*/@codeListValue))">
                        <dct:description>
                            <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                            <xsl:value-of select="."/>
                        </dct:description>
                    </xsl:if>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template match="gmd:onLine/*[not(gmd:function/*/@codeListValue = 'download')]/gmd:linkage/*">
        <dcat:accessURL rdf:resource="{iri-to-uri(.)}"/>
    </xsl:template>
    
    <xsl:template match="gmd:onLine/*[gmd:function/*/@codeListValue = 'download']/gmd:linkage/*">
        <dcat:accessURL rdf:resource="{iri-to-uri(.)}"/>
    </xsl:template>

    <xsl:template match="gmd:resourceConstraints/*[gmd:accessConstraints]/gmd:otherConstraints">
        <dct:rights>
            <dct:RightsStatement>
                <xsl:apply-templates select="*"/>
            </dct:RightsStatement>
        </dct:rights>
    </xsl:template>
    
    <xsl:template match="gmd:resourceConstraints/*[gmd:useConstraints]/gmd:otherConstraints">
        <dct:license>
            <dct:LicenseDocument>
                <xsl:apply-templates select="*"/>
            </dct:LicenseDocument>
        </dct:license>
    </xsl:template>

    <xsl:template match="*[count(gmd:resourceConstraints/*[gmd:useConstraints]/gmd:otherConstraints) &gt; 1]/gmd:resourceConstraints/*[gmd:useConstraints]/gmd:otherConstraints[starts-with(normalize-space(gco:CharacterString), '{')]" priority="2.0"/>

    <xsl:template match="gmd:otherConstraints/gco:CharacterString[not(../gmx:Anchor)]">
        <rdfs:label>
            <xsl:call-template name="xmlLang"/>
            <xsl:value-of select="."/>
        </rdfs:label>
        <xsl:apply-templates select="../gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString"/>
    </xsl:template>
    
    <xsl:template match="gmd:otherConstraints/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString">
        <xsl:variable name="localeRef" select="substring-after(@locale, '#')"/>
        <xsl:variable name="locale" select="ancestor::gmd:MD_Metadata/gmd:locale/*[@id = $localeRef]|ancestor::gmi:MI_Metadata/gmd:locale/*[@id = $localeRef]"/>
        <xsl:if test="$locale">
            <xsl:choose>
                <xsl:when test="ancestor::gmd:MD_Metadata/gmd:language/*/gco:CharacterString">
                    <xsl:variable name="languageCode" select="string(ancestor::gmd:MD_Metadata/gmd:language[1]/*/gco:CharacterString)"/>
                    <xsl:if test="not($languageCode = string($locale/gmd:languageCode/*/@codeListValue))">
                        <rdfs:label>
                            <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                            <xsl:value-of select="."/>
                        </rdfs:label>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="ancestor::gmd:MD_Metadata/gmd:language/*/@codeListValue">
                    <xsl:variable name="languageCode" select="string(ancestor::gmd:MD_Metadata/gmd:language[1]/*/@codeListValue)"/>
                    <xsl:if test="not($languageCode = string($locale/gmd:languageCode/*/@codeListValue))">
                        <rdfs:label>
                            <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                            <xsl:value-of select="."/>
                        </rdfs:label>
                    </xsl:if>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template match="gmd:otherConstraints/gmx:Anchor">
        <xsl:attribute name="rdf:about">
            <xsl:value-of select="iri-to-uri(@xlink:href)"/>
        </xsl:attribute>
    </xsl:template>
    
    <!-- 
    <xsl:template match="gmd:resourceConstraints/*/gmd:accessConstraints/*/@codeListValue">
        <dct:rights>
            <dct:RightsStatement>
                <rdfs:label>
                    <xsl:value-of select="iri-to-uri(.)"/>
                </rdfs:label>
            </dct:RightsStatement>
        </dct:rights>
    </xsl:template>

    <xsl:template match="gmd:resourceConstraints/*/gmd:accessConstraints/*[@codeListValue !='otherRestrictions']/@codeListValue">
        <dct:rights>
            <dct:RightsStatement>
                <rdfs:label>
                    <xsl:value-of select="."/>
                </rdfs:label>
            </dct:RightsStatement>
        </dct:rights>
    </xsl:template>
-->
    <xsl:template match="gmd:distributionFormat/*/gmd:name/gmx:Anchor | gmd:distributorFormat/*/gmd:name/gmx:Anchor">
        <dct:format>
            <dct:MediaType rdf:about="{iri-to-uri(@xlink:href)}"/>
        </dct:format>
    </xsl:template>
    
    <xsl:template match="gmd:distributionFormat/*/gmd:name/gco:CharacterString | gmd:distributorFormat/*/gmd:name/gco:CharacterString">
        <xsl:variable name="formatName" select="text()"/>
        <xsl:variable name="formatNameUC" select="upper-case($formatName)"/>
        <xsl:variable name="formatNameUC">
            <xsl:choose>
                <xsl:when test="$formatNameUC = 'GEOTIFF'">TIFF</xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$formatNameUC"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable> 
        <xsl:variable name="mdrUri" select="$mdrFileTypes/*/skos:Concept[*/text() = $formatNameUC]/@rdf:about"/>
        <xsl:choose>
            <xsl:when test="$mdrUri">
                <dct:format>
                    <dct:MediaType rdf:about="{$mdrUri}"/>
                </dct:format>
            </xsl:when>
            <xsl:when test="$ianaMediaTypes/registry/file[text() = $formatName]">
                <dcat:mediaType>
                    <dct:MediaType rdf:about="{concat('https://www.iana.org/assignments/media-types/', encode-for-uri($formatName))}"/>
                </dcat:mediaType>
            </xsl:when>
<!-- omit the format if it is unknown
            <xsl:otherwise>
                <dct:format rdf:parseType="Resource">
                    <rdfs:label><xsl:value-of select="."/></rdfs:label>
                </dct:format>
            </xsl:otherwise>
-->
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="gmd:spatialRepresentationType/*/@codeListValue">
        <adms:representationTechnique rdf:resource="{concat($inspire_md_codelist, 'SpatialRepresentationType/', encode-for-uri(.))}"/>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/gmd:citation/*/gmd:identifier/*/gmd:code/*">
        <dct:identifier>
            <xsl:apply-templates select="." mode="identifier"/>
        </dct:identifier>
    </xsl:template>

    <xsl:template match="gmd:identifier/*[not(gmd:codeSpace)]/gmd:code/*" mode="identifier">
        <xsl:apply-templates select="." mode="identifierType"/>
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="gmd:identifier/*[gmd:codeSpace and substring(gmd:codeSpace/gco:CharacterString,string-length(gmd:codeSpace/gco:CharacterString),1) != '/']/gmd:code/gco:CharacterString" mode="identifier">
        <xsl:apply-templates select="../../gmd:codeSpace/gco:CharacterString" mode="identifierType"/>
        <xsl:call-template name="replace">
            <xsl:with-param name="str" select="concat(../../gmd:codeSpace/gco:CharacterString, '/', ../../gmd:code/gco:CharacterString)"/>
            <xsl:with-param name="from" select="' '"/>
            <xsl:with-param name="to" select="'%20'"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="gmd:identifier/*[gmd:codeSpace and substring(gmd:codeSpace/gco:CharacterString,string-length(gmd:codeSpace/gco:CharacterString),1) = '/']/gmd:code/gco:CharacterString" mode="identifier">
        <xsl:apply-templates select="../../gmd:codeSpace/gco:CharacterString" mode="identifierType"/>
        <xsl:call-template name="replace">
            <xsl:with-param name="str" select="concat(../../gmd:codeSpace/gco:CharacterString, ../../gmd:code/gco:CharacterString)"/>
            <xsl:with-param name="from" select="' '"/>
            <xsl:with-param name="to" select="'%20'"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="*[starts-with(text(), 'http://') or starts-with(text(), 'https://')]" mode="identifierType">
        <xsl:attribute name="rdf:datatype">http://www.w3.org/2001/XMLSchema#anyURI</xsl:attribute>
    </xsl:template>

    <xsl:template match="*[not(starts-with(text(), 'http://') or starts-with(text(), 'https://'))]" mode="identifierType">
        <xsl:attribute name="rdf:datatype">http://www.w3.org/2001/XMLSchema#string</xsl:attribute>
    </xsl:template>
    
    <xsl:template name="replace">
        <xsl:param name="str"/>
        <xsl:param name="from"/>
        <xsl:param name="to"/>
        <xsl:choose>
            <xsl:when test="contains($str, $from)">
                <xsl:value-of select="concat(substring-before($str, $from), $to)"/>
                <xsl:call-template name="replace">
                    <xsl:with-param name="str" select="substring-after($str, $from)"/>
                    <xsl:with-param name="from" select="$from"/>
                    <xsl:with-param name="to" select="$to"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$str"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="gmd:parentIdentifier/*[text()]">
        <dct:isPartOf rdf:parseType="Resource">
            <dct:identifier rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                <xsl:value-of select="."/>
            </dct:identifier>
        </dct:isPartOf>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:descriptiveKeywords/*[not(gmd:thesaurusName)]/gmd:keyword/gco:CharacterString">
        <xsl:call-template name="dcatKeyword"/>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:descriptiveKeywords/*[starts-with(gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString, 'GEMET - INSPIRE themes')]/gmd:keyword[position() &gt; 1]/gco:CharacterString">
        <xsl:call-template name="dcatKeyword"/>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:descriptiveKeywords/*[starts-with(gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString, 'GEMET - INSPIRE themes')]/gmd:keyword[1]/gco:CharacterString">
        <xsl:variable name="themeLabel" select="text()"/>
        <xsl:variable name="themeUri" select="$inspireThemes/rdf:RDF/rdf:Description[dct:title = $themeLabel]/@rdf:about"/>
        <xsl:choose>
            <xsl:when test="$themeUri">
                <xsl:call-template name="themeSkos"/>
                <xsl:call-template name="euroVocDomain">
                    <xsl:with-param name="inspireTheme" select="$themeUri"/>
                </xsl:call-template>
                <!-- EruoVoc mapping deactivated
                <xsl:variable name="euroVocUri" select="$euroVocMapping/rdf:RDF/rdf:Description[skos:exactMatch/@rdf:resource=$themeUri]/@rdf:about"/>
                <xsl:choose>
                    <xsl:when test="$euroVocUri">
                        <dcat:theme rdf:resource="{$euroVocUri}"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:variable name="broaderEuroVocUri" select="$euroVocMapping/rdf:RDF/rdf:Description[skos:narrowMatch/@rdf:resource=$themeUri]/@rdf:about"/>
                        <xsl:if test="$broaderEuroVocUri">
                            <dcat:theme rdf:resource="{$broaderEuroVocUri}"/>
                        </xsl:if>
                    </xsl:otherwise>
                </xsl:choose>
                -->
            </xsl:when>
            <xsl:when test="$extended">
                <xsl:call-template name="themeSkos"/>
                <xsl:apply-templates select="ancestor::gmd:MD_Metadata/gmd:identificationInfo/*/gmd:topicCategory/*|ancestor::gmi:MI_Metadata/gmd:identificationInfo/*/gmd:topicCategory/*" mode="dcatTheme"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="dcatKeyword"/>
                <xsl:apply-templates select="ancestor::gmd:MD_Metadata/gmd:identificationInfo/*/gmd:topicCategory/*|ancestor::gmi:MI_Metadata/gmd:identificationInfo/*/gmd:topicCategory/*" mode="dcatTheme"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:descriptiveKeywords/*[gmd:thesaurusName and not(starts-with(gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString, 'GEMET - INSPIRE themes'))]/gmd:keyword/gco:CharacterString">
        <xsl:choose>
            <xsl:when test="$extended">
                <xsl:call-template name="themeSkos"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="dcatKeyword"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="dcatKeyword">
        <dcat:keyword>
            <xsl:apply-templates select="." mode="dcatKeyword"/>
        </dcat:keyword>
        <xsl:apply-templates select="../gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString"/>
    </xsl:template>
    
    <xsl:template match="gmx:Anchor" mode="dcatKeyword">
        <xsl:attribute name="rdf:resource">
            <xsl:value-of select="iri-to-uri(@xlink:href)"/>
        </xsl:attribute>
    </xsl:template>
    
    <xsl:template match="*" mode="dcatKeyword">
        <xsl:call-template name="xmlLang"/>
        <xsl:value-of select="."/>
    </xsl:template>
    
    <xsl:template name="themeSkos">
        <dcat:theme>
            <skos:Concept>
                <skos:prefLabel><xsl:value-of select="."/></skos:prefLabel>
                <skos:inScheme>
                    <skos:ConceptScheme>
                        <dct:title>
                            <xsl:call-template name="xmlLang"/>
                            <xsl:value-of select="../../gmd:thesaurusName/*/gmd:title/*"/>
                        </dct:title>
                        <xsl:apply-templates select="../../gmd:thesaurusName/*/gmd:date/*[gmd:dateType/*/@codeListValue='creation' or gmd:dateType/*/@codeListValue='publication']/gmd:date/*"/>
                    </skos:ConceptScheme>
                </skos:inScheme>
            </skos:Concept>
        </dcat:theme>
    </xsl:template>
    
    <xsl:template match="gmd:keyword/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString">
        <xsl:variable name="localeRef" select="substring-after(@locale, '#')"/>
        <xsl:variable name="locale" select="ancestor::gmd:MD_Metadata/gmd:locale/*[@id = $localeRef]|ancestor::gmi:MI_Metadata/gmd:locale/*[@id = $localeRef]"/>
        <xsl:if test="$locale">
            <dcat:keyword>
                <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                <xsl:value-of select="."/>
            </dcat:keyword>
        </xsl:if>
    </xsl:template>

    <xsl:template match="gmd:dataQualityInfo/*/gmd:lineage/*/gmd:statement/gco:CharacterString">
        <dct:provenance>
            <dct:ProvenanceStatement>
                <rdfs:label>
                    <xsl:call-template name="xmlLang"/>
                    <xsl:value-of select="."/>
                </rdfs:label>
                <xsl:apply-templates select="../gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString"/>
            </dct:ProvenanceStatement>
        </dct:provenance>
    </xsl:template>

    <xsl:template match="gmd:dataQualityInfo/*/gmd:lineage/*/gmd:statement/gmd:PT_FreeText/gmd:textGroup/gmd:LocalisedCharacterString">
        <xsl:variable name="localeRef" select="substring-after(@locale, '#')"/>
        <xsl:variable name="locale" select="ancestor::gmd:MD_Metadata/gmd:locale/*[@id = $localeRef]|ancestor::gmi:MI_Metadata/gmd:locale/*[@id = $localeRef]"/>
        <xsl:if test="$locale">
            <xsl:choose>
                <xsl:when test="ancestor::gmd:MD_Metadata/gmd:language/*/gco:CharacterString">
                    <xsl:variable name="languageCode" select="string(ancestor::gmd:MD_Metadata/gmd:language[1]/*/gco:CharacterString)"/>
                    <xsl:if test="not($languageCode = string($locale/gmd:languageCode/*/@codeListValue))">
                        <rdfs:label>
                            <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                            <xsl:value-of select="."/>
                        </rdfs:label>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="ancestor::gmd:MD_Metadata/gmd:language/*/@codeListValue">
                    <xsl:variable name="languageCode" select="string(ancestor::gmd:MD_Metadata/gmd:language[1]/*/@codeListValue)"/>
                    <xsl:if test="not($languageCode = string($locale/gmd:languageCode/*/@codeListValue))">
                        <rdfs:label>
                            <xsl:apply-templates select="$locale/gmd:languageCode/*" mode="xmlLang"/>
                            <xsl:value-of select="."/>
                        </rdfs:label>
                    </xsl:if>
                </xsl:when>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:topicCategory/*">
        <dct:subject rdf:resource="{concat($inspire_md_codelist, 'TopicCategory/', encode-for-uri(.))}"/>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/srv:operatesOn/@uuidref|gmd:identificationInfo/*/srv:operatesOn/@uriref">
        <xsl:choose>
            <xsl:when test="starts-with(., 'http:') or starts-with(., 'https:')">
                <dcat:servesDataset>
                    <dcat:Dataset rdf:about="{encode-for-uri(.)}"/>
                </dcat:servesDataset>
            </xsl:when>
            <xsl:otherwise>
                <dcat:servesDataset>
                    <dcat:Dataset>
                        <dct:identifier rdf:datatype="http://www.w3.org/2001/XMLSchema#string">
                            <xsl:value-of select="."/>
                        </dct:identifier>
                    </dcat:Dataset>
                </dcat:servesDataset>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="gmd:referenceSystemInfo/*/gmd:referenceSystemIdentifier/*[gmd:code/gmx:Anchor/@xlink:href]">
        <dct:conformsTo>
            <rdf:Description rdf:about="{iri-to-uri(gmd:code/gmx:Anchor/@xlink:href)}">
                <rdf:type rdf:resource="http://purl.org/dc/terms/Standard"/>
                <dct:type rdf:resource="{concat($inspire_md_codelist, 'SpatialReferenceSystem')}"/>
            </rdf:Description>
        </dct:conformsTo>
    </xsl:template>
    
    <xsl:template match="gmd:referenceSystemInfo/*/gmd:referenceSystemIdentifier/*[starts-with(gmd:code/gco:CharacterString, 'http:') or starts-with(gmd:code/gco:CharacterString, 'https:')]">
        <dct:conformsTo>
            <rdf:Description rdf:about="{iri-to-uri(gmd:code/gco:CharacterString)}">
                <rdf:type rdf:resource="http://purl.org/dc/terms/Standard"/>
                <dct:type rdf:resource="{concat($inspire_md_codelist, 'SpatialReferenceSystem')}"/>
            </rdf:Description>
        </dct:conformsTo>
    </xsl:template>
    
    <xsl:template match="gmd:referenceSystemInfo/*/gmd:referenceSystemIdentifier/*[starts-with(gmd:code/gco:CharacterString, 'urn:')]">
        <xsl:variable name="srid" select="substring-after(substring-after(substring-after(substring-after(substring-after(substring-after(gmd:code/gco:CharacterString,':'),':'),':'),':'),':'),':')"/>
        <xsl:choose>
            <xsl:when test="$srid != '' and string(number($srid)) != ''">
                <dct:conformsTo>
                    <rdf:Description rdf:about="{concat($epsgRegister, '/', encode-for-uri($srid))}">
                        <rdf:type rdf:resource="http://purl.org/dc/terms/Standard"/>
                        <rdf:type rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
                        <dct:type rdf:resource="{concat($inspire_md_codelist, 'SpatialReferenceSystem')}"/>
                        <dct:identifier rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI"><xsl:value-of select="gmd:code/gco:CharacterString"/></dct:identifier>
                        <skos:inScheme>
                            <skos:ConceptScheme rdf:about="{$epsgRegister}">
                                <dct:title xml:lang="en">EPSG Coordinate Reference Systems</dct:title>
                            </skos:ConceptScheme>
                        </skos:inScheme>
                        <xsl:variable name="version" select="substring-before(substring-after(substring-after(substring-after(substring-after(substring-after(gmd:code/gco:CharacterString,':'),':'),':'),':'),':'),':')"/>
                        <xsl:if test="$version != ''">
                            <owl:versionInfo><xsl:value-of select="$version"/></owl:versionInfo>
                        </xsl:if>
                    </rdf:Description>
                </dct:conformsTo>
            </xsl:when>
            <xsl:otherwise>
                <dct:conformsTo rdf:parseType="Resource">
                    <rdf:type rdf:resource="http://purl.org/dc/terms/Standard"/>
                    <rdf:type rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
                    <dct:type rdf:resource="{concat($inspire_md_codelist, 'SpatialReferenceSystem')}"/>
                    <dct:identifier rdf:datatype="http://www.w3.org/2001/XMLSchema#anyURI"><xsl:value-of select="gmd:code/gco:CharacterString"/></dct:identifier>
                    <xsl:if test="gmd:codeSpace/gco:CharacterString != ''">
                        <skos:inScheme>
                            <skos:ConceptScheme>
                                <dct:title>
                                    <xsl:call-template name="xmlLang"/>
                                    <xsl:value-of select="gmd:codeSpace/gco:CharacterString"/>
                                </dct:title>
                            </skos:ConceptScheme>
                        </skos:inScheme>
                    </xsl:if>
                    <xsl:if test="gmd:version/gco:CharacterString != ''">
                        <owl:versionInfo><xsl:value-of select="gmd:version/gco:CharacterString"/></owl:versionInfo>
                    </xsl:if>
                </dct:conformsTo>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="gmd:referenceSystemInfo/*/gmd:referenceSystemIdentifier/*[not(gmd:code/gmx:Anchor/@xlink:href != '' or starts-with(gmd:code/gco:CharacterString, 'http:') or starts-with(gmd:code/gco:CharacterString, 'https:') or starts-with(gmd:code/gco:CharacterString, 'urn:'))]">
        <dct:conformsTo rdf:parseType="Resource">
            <rdf:type rdf:resource="http://purl.org/dc/terms/Standard"/>
            <rdf:type rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
            <dct:type rdf:resource="{concat($inspire_md_codelist, 'SpatialReferenceSystem')}"/>
            <skos:prefLabel>
                <xsl:call-template name="xmlLang"/>
                <xsl:value-of select="gmd:code/gco:CharacterString"/>
            </skos:prefLabel>
            <xsl:if test="gmd:codeSpace/gco:CharacterString != ''">
                <skos:inScheme>
                    <skos:ConceptScheme>
                        <dct:title>
                            <xsl:call-template name="xmlLang"/>
                            <xsl:value-of select="gmd:codeSpace/gco:CharacterString"/>
                        </dct:title>
                    </skos:ConceptScheme>
                </skos:inScheme>
            </xsl:if>
            <xsl:if test="gmd:version/gco:CharacterString != ''">
                <owl:versionInfo>
                    <xsl:call-template name="xmlLang"/>
                    <xsl:value-of select="gmd:version/gco:CharacterString"/>
                </owl:versionInfo>
            </xsl:if>
        </dct:conformsTo>
    </xsl:template>

    <xsl:template match="gmd:maintenanceAndUpdateFrequency/*/@codeListValue[.='continual' or .='fortnightly' or .='biannually' or .='annually' or .='irregular' or .='daily' or .='weekly' or .='monthly' or .='quarterly' or .='unknown' or .='asNeeded' or .='notPlanned']">
        <dct:accrualPeriodicity>
            <dct:Frequency>
                <xsl:attribute name="rdf:about">
                    <xsl:choose>
                        <xsl:when test=". = 'asNeeded' or . = 'notPlanned'">
                            <xsl:value-of select="concat($inspire_md_codelist, 'MaintenanceFrequency/')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'http://publications.europa.eu/resource/authority/frequency/'"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:apply-templates select="." mode="frequency"/>
                </xsl:attribute>
            </dct:Frequency>
        </dct:accrualPeriodicity>
    </xsl:template>

    <xsl:template match="gmd:maintenanceAndUpdateFrequency/*/@codeListValue[.='continual']" mode="frequency">
        <xsl:value-of select="'CONT'"/>
    </xsl:template>

    <xsl:template match="gmd:maintenanceAndUpdateFrequency/*/@codeListValue[.='fortnightly']" mode="frequency">
        <xsl:value-of select="'BIWEEKLY'"/>
    </xsl:template>

    <xsl:template match="gmd:maintenanceAndUpdateFrequency/*/@codeListValue[.='biannually']" mode="frequency">
        <xsl:value-of select="'ANNUAL_2'"/>
    </xsl:template>

    <xsl:template match="gmd:maintenanceAndUpdateFrequency/*/@codeListValue[.='annually']" mode="frequency">
        <xsl:value-of select="'ANNUAL'"/>
    </xsl:template>

    <xsl:template match="gmd:maintenanceAndUpdateFrequency/*/@codeListValue[.='irregular']" mode="frequency">
        <xsl:value-of select="'IRREG'"/>
    </xsl:template>

    <xsl:template match="gmd:maintenanceAndUpdateFrequency/*/@codeListValue[.='daily']" mode="frequency">
        <xsl:value-of select="'DAILY'"/>
    </xsl:template>

    <xsl:template match="gmd:maintenanceAndUpdateFrequency/*/@codeListValue[.='weekly']" mode="frequency">
        <xsl:value-of select="'WEEKLY'"/>
    </xsl:template>

    <xsl:template match="gmd:maintenanceAndUpdateFrequency/*/@codeListValue[.='monthly']" mode="frequency">
        <xsl:value-of select="'MONTHLY'"/>
    </xsl:template>

    <xsl:template match="gmd:maintenanceAndUpdateFrequency/*/@codeListValue[.='quarterly']" mode="frequency">
        <xsl:value-of select="'QUARTERLY'"/>
    </xsl:template>

    <xsl:template match="gmd:maintenanceAndUpdateFrequency/*/@codeListValue[.='unknown']" mode="frequency">
        <xsl:value-of select="'UNKNOWN'"/>
    </xsl:template>
    
    <xsl:template match="gmd:maintenanceAndUpdateFrequency/*/@codeListValue[.='asNeeded' or .='notPlanned']" mode="frequency">
        <xsl:value-of select="."/>
    </xsl:template>
    
    <xsl:template match="gmd:identificationInfo/*/gmd:topicCategory/*" mode="dcatTheme">
        <xsl:call-template name="euroVocDomain">
            <xsl:with-param name="inspireTheme" select="text()"/>
        </xsl:call-template>
    </xsl:template>
    
    <!-- mapping of INSPIRE themes / ISO topicCategory to the EuroVoc domain and categorization topic -->
    <xsl:template name="euroVocDomain">
        <xsl:param name="inspireTheme"/>
        <xsl:choose>
            <xsl:when test="$inspireTheme = 'http://inspire.ec.europa.eu/theme/ad' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/oi' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/rs' or
                $inspireTheme = 'intelligenceMilitary' or $inspireTheme = 'imageryBaseMapsEarthCover'
                or $inspireTheme = 'location'">
                <!-- there is no mapping for these themes -->
            </xsl:when>
            <xsl:when test="$inspireTheme = 'http://inspire.ec.europa.eu/theme/gn'">
                <!-- regions, cities -->
<!--
                <dcat:keyword xml:lang="en">Regions; Cities</dcat:keyword>
-->
                <dcat:theme rdf:resource="http://publications.europa.eu/resource/authority/data-theme/REGI"/>
            </xsl:when>
            <xsl:when test="$inspireTheme = 'http://inspire.ec.europa.eu/theme/ac' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/am' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/br' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/ef' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/el' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/er' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/hb' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/lc' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/mr' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/nz' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/of' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/ps' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/sr' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/sd' or
                $inspireTheme = 'biota' or $inspireTheme = 'climatologyMeteorologyAtmosphere' or
                $inspireTheme = 'elevation' or $inspireTheme = 'environment' or
                $inspireTheme = 'inlandWaters' or
                $inspireTheme = 'oceans'">
                <!-- environment -->
                <dcat:theme rdf:resource="http://publications.europa.eu/resource/authority/data-theme/ENVI"/>
<!--
                <dcat:theme rdf:resource="http://eurovoc.europa.eu/100155"/>
                <dcat:keyword xml:lang="en">52 ENVIRONMENT</dcat:keyword>
                <dcat:keyword xml:lang="en">Environment</dcat:keyword>
-->
            </xsl:when>
            <xsl:when test="$inspireTheme = 'http://inspire.ec.europa.eu/theme/af' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/lu' or
                $inspireTheme = 'farming'">
                <!-- agriculture, forestry and fishery -->
                <dcat:theme rdf:resource="http://publications.europa.eu/resource/authority/data-theme/AGRI"/>
<!--
                <dcat:theme rdf:resource="http://eurovoc.europa.eu/100156"/>
                <dcat:keyword xml:lang="en">56 AGRICULTURE, FORESTRY AND FISHERIES</dcat:keyword>
                <dcat:keyword xml:lang="en">Agriculture, Forestry and fisheries</dcat:keyword>
                <dcat:keyword xml:lang="en">Agriculture, Fisheries, Forestry, foods</dcat:keyword>
-->
            </xsl:when>
            <xsl:when test="$inspireTheme = 'http://inspire.ec.europa.eu/theme/au' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/us' or
                $inspireTheme = 'boundaries'">
                <!-- politics -->
                <dcat:theme rdf:resource="http://publications.europa.eu/resource/authority/data-theme/GOVE"/>
<!--
                <dcat:theme rdf:resource="http://eurovoc.europa.eu/100142"/>
                <dcat:keyword xml:lang="en">04 POLITICS</dcat:keyword>
                <dcat:keyword xml:lang="en">Politics</dcat:keyword>
                <dcat:keyword xml:lang="en">Government, Public sector</dcat:keyword>
-->
            </xsl:when>
            <xsl:when test="$inspireTheme = 'http://inspire.ec.europa.eu/theme/bu' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/pf' or
                $inspireTheme = 'utilitiesCommunication' or $inspireTheme = 'structure'">
                <!-- industry -->
                <dcat:theme rdf:resource="http://publications.europa.eu/resource/authority/data-theme/ECON"/>
<!--
                <dcat:theme rdf:resource="http://eurovoc.europa.eu/100160"/>
                <dcat:keyword xml:lang="en">68 INDUSTRY</dcat:keyword>
                <dcat:keyword xml:lang="en">Industry</dcat:keyword>
                <dcat:keyword xml:lang="en">Economy and finance</dcat:keyword>
-->
            </xsl:when>
            <xsl:when test="$inspireTheme = 'http://inspire.ec.europa.eu/theme/cp' or
                $inspireTheme = 'planningCadastre'">
                <!-- law -->
                <dcat:theme rdf:resource="http://publications.europa.eu/resource/authority/data-theme/JUST"/>
<!--
                <dcat:theme rdf:resource="http://eurovoc.europa.eu/100145"/>
                <dcat:keyword xml:lang="en">12 LAW</dcat:keyword>
                <dcat:keyword xml:lang="en">Law</dcat:keyword>
                <dcat:keyword xml:lang="en">Justice, legal system, public safety</dcat:keyword>
-->
            </xsl:when>
            <xsl:when test="$inspireTheme = 'http://inspire.ec.europa.eu/theme/hh' or
                $inspireTheme = 'health'">
                <!-- health -->
                <dcat:theme rdf:resource="http://publications.europa.eu/resource/authority/data-theme/HEAL"/>
<!-- 
                <dcat:theme rdf:resource="http://eurovoc.europa.eu/100149"/>

-->
            </xsl:when>
            <xsl:when test="$inspireTheme = 'http://inspire.ec.europa.eu/theme/pd' or
                $inspireTheme = 'society'">
                <!-- social questions -->
                <dcat:theme rdf:resource="http://publications.europa.eu/resource/authority/data-theme/SOCI"/>
<!--
                <dcat:theme rdf:resource="http://eurovoc.europa.eu/100149"/>
                <dcat:keyword xml:lang="en">28 SOCIAL QUESTIONS</dcat:keyword>
                <dcat:keyword xml:lang="en">Social Questions</dcat:keyword>
                <dcat:keyword xml:lang="en">Population and social conditions</dcat:keyword>
-->
            </xsl:when>
            <xsl:when test="$inspireTheme = 'http://inspire.ec.europa.eu/theme/ge' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/gg' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/hy' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/mf' or
                $inspireTheme = 'http://inspire.ec.europa.eu/theme/so' or
                $inspireTheme = 'geoscientificInformation'">
                <!-- sciences -->
                <dcat:theme rdf:resource="http://publications.europa.eu/resource/authority/data-theme/TECH"/>
<!--
                <dcat:theme rdf:resource="http://eurovoc.europa.eu/100151"/>
                <dcat:keyword xml:lang="en">36 SCIENCE</dcat:keyword>
                <dcat:keyword xml:lang="en">Science</dcat:keyword>
                <dcat:keyword xml:lang="en">Science and technology</dcat:keyword>
-->
            </xsl:when>
            <xsl:when test="$inspireTheme = 'http://inspire.ec.europa.eu/theme/su' or
                $inspireTheme = 'economy'">
                <!-- economics -->
                <dcat:theme rdf:resource="http://publications.europa.eu/resource/authority/data-theme/ECON"/>
<!--
                <dcat:theme rdf:resource="http://eurovoc.europa.eu/100146"/>
                <dcat:keyword xml:lang="en">16 ECONOMICS</dcat:keyword>
                <dcat:keyword xml:lang="en">Economics</dcat:keyword>
                <dcat:keyword xml:lang="en">Economy and finance</dcat:keyword>
-->
            </xsl:when>
            <xsl:when test="$inspireTheme = 'http://inspire.ec.europa.eu/theme/tn' or
                $inspireTheme = 'transportation'">
                <!-- transport -->
                <dcat:theme rdf:resource="http://publications.europa.eu/resource/authority/data-theme/TRAN"/>
<!--
                <dcat:theme rdf:resource="http://eurovoc.europa.eu/100154"/>
                <dcat:keyword xml:lang="en">48 TRANSPORT</dcat:keyword>
                <dcat:keyword xml:lang="en">Transport</dcat:keyword>
-->
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='ucs2']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-10646-UCS-2</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='ucs4']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-10646-UCS-4</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='utf7']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">UTF-7</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='utf8']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">UTF-8</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='utf16']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">UTF-16</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part1']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-1</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part2']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-2</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part3']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-3</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part4']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-4</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part5']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-5</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part6']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-6</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part7']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-7</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part8']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-8</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part9']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-9</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part10']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-10</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part11']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-11</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part12']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-12</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part13']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-13</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part14']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-14</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part15']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-15</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='8859part16']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO-8859-16</cnt:characterEncoding>
    </xsl:template>
    
    <xsl:template match="gmd:characterSet/*/@codeListValue[.='jis']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">JIS_Encoding</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='shiftJIS']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Shift_JIS</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='eucJP']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">EUC-JP</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='usAscii']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">US-ASCII</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='ebcdic']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">IBM037</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='eucKR']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">EUC-KR</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='big5']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Big5</cnt:characterEncoding>
    </xsl:template>

    <xsl:template match="gmd:characterSet/*/@codeListValue[.='GB2312']">
        <cnt:characterEncoding rdf:datatype="http://www.w3.org/2001/XMLSchema#string">GB2312</cnt:characterEncoding>
    </xsl:template>

    <xsl:template name="xmlLang">
        <xsl:apply-templates select="ancestor::gmd:MD_Metadata/gmd:language/*[@codeListValue]|ancestor::gmd:MD_Metadata/gmd:language/gco:CharacterString|
            ancestor::gmi:MI_Metadata/gmd:language/*[@codeListValue]|ancestor::gmi:MI_Metadata/gmd:language/gco:CharacterString" mode="xmlLang"/>
    </xsl:template>

    <xsl:template match="gmd:language/*[@codeListValue]">
        <xsl:call-template name="linguisticSystem">
            <xsl:with-param name="lang" select="@codeListValue"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:language/*[not(@codeListValue)]">
        <xsl:call-template name="linguisticSystem">
            <xsl:with-param name="lang" select="."/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template name="linguisticSystem">
        <xsl:param name="lang"/>
        <xsl:variable name="code">
            <xsl:choose>
                <xsl:when test="$lang = 'ger'">deu</xsl:when>
                <xsl:when test="$lang = 'cze'">ces</xsl:when>
                <xsl:when test="$lang = 'dut'">nld</xsl:when>
                <xsl:when test="$lang = 'rum'">ron</xsl:when>
                <xsl:when test="$lang = 'fre'">fra</xsl:when>
                <xsl:when test="$lang = 'slo'">slk</xsl:when>
                <xsl:when test="$lang = 'gre'">ell</xsl:when>
                <xsl:when test="$lang = 'ice'">isl</xsl:when>
                <xsl:otherwise><xsl:value-of select="$lang"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <dct:language>
            <dct:LinguisticSystem rdf:about="{concat('http://publications.europa.eu/resource/authority/language/', upper-case($code))}"/>
        </dct:language>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='bul']|gmd:language/gco:CharacterString[text()='bul']" mode="xmlLang">
        <xsl:attribute name="xml:lang">bg</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='gle']|gmd:language/gco:CharacterString[text()='gle']" mode="xmlLang">
        <xsl:attribute name="xml:lang">ga</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='hrv']|gmd:language/gco:CharacterString[text()='hrv']" mode="xmlLang">
        <xsl:attribute name="xml:lang">hr</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='ita']|gmd:language/gco:CharacterString[text()='ita']" mode="xmlLang">
        <xsl:attribute name="xml:lang">it</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='cze']|gmd:language/gco:CharacterString[text()='cze']" mode="xmlLang">
        <xsl:attribute name="xml:lang">cs</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='lav']|gmd:language/gco:CharacterString[text()='lav']" mode="xmlLang">
        <xsl:attribute name="xml:lang">lv</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='dan']|gmd:language/gco:CharacterString[text()='dan']" mode="xmlLang">
        <xsl:attribute name="xml:lang">da</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='lit']|gmd:language/gco:CharacterString[text()='lit']" mode="xmlLang">
        <xsl:attribute name="xml:lang">lt</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='dut']|gmd:language/gco:CharacterString[text()='dut']" mode="xmlLang">
        <xsl:attribute name="xml:lang">nl</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='mlt']|gmd:language/gco:CharacterString[text()='mlt']" mode="xmlLang">
        <xsl:attribute name="xml:lang">mt</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='eng']|gmd:language/gco:CharacterString[text()='eng']" mode="xmlLang">
        <xsl:attribute name="xml:lang">en</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='pol']|gmd:language/gco:CharacterString[text()='pol']" mode="xmlLang">
        <xsl:attribute name="xml:lang">pl</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='est']|gmd:language/gco:CharacterString[text()='est']" mode="xmlLang">
        <xsl:attribute name="xml:lang">et</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='por']|gmd:language/gco:CharacterString[text()='por']" mode="xmlLang">
        <xsl:attribute name="xml:lang">pt</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='fin']|gmd:language/gco:CharacterString[text()='fin']" mode="xmlLang">
        <xsl:attribute name="xml:lang">fi</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='rum']|gmd:language/gco:CharacterString[text()='rum']" mode="xmlLang">
        <xsl:attribute name="xml:lang">ro</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='fre']|gmd:language/gco:CharacterString[text()='fre']" mode="xmlLang">
        <xsl:attribute name="xml:lang">fr</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='slo']|gmd:language/gco:CharacterString[text()='slo']" mode="xmlLang">
        <xsl:attribute name="xml:lang">sk</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='ger']|gmd:language/gco:CharacterString[text()='ger']" mode="xmlLang">
        <xsl:attribute name="xml:lang">de</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='slv']|gmd:language/gco:CharacterString[text()='slv']" mode="xmlLang">
        <xsl:attribute name="xml:lang">sl</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='gre']|gmd:language/gco:CharacterString[text()='gre']" mode="xmlLang">
        <xsl:attribute name="xml:lang">el</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='spa']|gmd:language/gco:CharacterString[text()='spa']" mode="xmlLang">
        <xsl:attribute name="xml:lang">es</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='hun']|gmd:language/gco:CharacterString[text()='hun']" mode="xmlLang">
        <xsl:attribute name="xml:lang">hu</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode[@codeListValue='swe']|gmd:language/gco:CharacterString[text()='swe']" mode="xmlLang">
        <xsl:attribute name="xml:lang">sv</xsl:attribute>
    </xsl:template>

    <xsl:template match="gmd:LanguageCode/*[@codeListValue='ice']|gmd:language/gco:CharacterString[text()='ice']" mode="xmlLang">
        <xsl:attribute name="xml:lang">is</xsl:attribute>
    </xsl:template>

    <xsl:template priority="0" match="gmd:LanguageCode[@codeListValue]" mode="xmlLang">
        <xsl:attribute name="xml:lang"><xsl:value-of select="@codeListValue"/></xsl:attribute>
    </xsl:template>
    
    <xsl:template priority="0" match="gmd:language/gco:CharacterString" mode="xmlLang">
        <xsl:attribute name="xml:lang"><xsl:value-of select="."/></xsl:attribute>
    </xsl:template>
    
    <xsl:template match="node()|@*"/>
    
</xsl:stylesheet>
