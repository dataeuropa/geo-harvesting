<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- transforms a CSW GetRecordsResponse to an OAI-PMH response -->
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gml="http://www.opengis.net/gml"
                xmlns:atom="http://www.w3.org/2005/Atom" xmlns:os="http://a9.com/-/spec/opensearch/1.1/"
                xmlns:csw="http://www.opengis.net/cat/csw/2.0.2" xmlns:gmd="http://www.isotc211.org/2005/gmd"
                xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:gmi="http://www.isotc211.org/2005/gmi"
                exclude-result-prefixes="gmd gml dc csw atom os gmi">
    <xsl:import href="oai-pmhUtil.xsl"/>
    <xsl:import href="iso2dcat.xsl"/>

    <xsl:output method="xml"/>

    <!-- request parameters -->
    <xsl:param name="verb"/>
    <xsl:param name="identifier"/>
    <xsl:param name="metadataPrefix"/>
    <xsl:param name="from"/>
    <xsl:param name="until"/>
    <xsl:param name="set"/>
    <xsl:param name="resumptionToken"/>

    <xsl:param name="response_date"/>

    <!--
    <xsl:strip-space elements="*"/>
    -->
    <xsl:template match="atom:feed">
        <OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/"
                 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                 xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
            <responseDate><xsl:value-of select="$response_date"/></responseDate>
            <request>
                <xsl:call-template name="requestAttributes"/>
                <xsl:value-of select="$harvester_base_url"/>
            </request>
            <xsl:choose>
                <xsl:when test="$verb = 'ListRecords'">
                    <ListRecords>
                        <xsl:apply-templates select="atom:entry/gmd:MD_Metadata|atom:entry/gmi:MI_Metadata" mode="record"/>
                        <xsl:call-template name="resumptionToken"/>
                    </ListRecords>
                </xsl:when>
                <xsl:when test="$verb = 'ListIdentifiers'">
                    <ListIdentifiers>
                        <xsl:apply-templates select="atom:entry/gmd:MD_Metadata|atom:entry/gmi:MI_Metadata" mode="header"/>
                        <xsl:call-template name="resumptionToken"/>
                    </ListIdentifiers>
                </xsl:when>
                <xsl:otherwise>
                    <GetRecord>
                        <xsl:apply-templates select="atom:entry/gmd:MD_Metadata|atom:entry/gmi:MI_Metadata" mode="record"/>
                    </GetRecord>
                </xsl:otherwise>
            </xsl:choose>
        </OAI-PMH>
    </xsl:template>

    <xsl:template name="resumptionToken">
        <xsl:variable name="numberOfRecordsMatched" select="number(os:totalResults)"/>
        <xsl:choose>
            <xsl:when test="not($resumptionToken) and count(atom:entry) &lt; $numberOfRecordsMatched">
                <resumptionToken xmlns="http://www.openarchives.org/OAI/2.0/" cursor="0" completeListSize="{$numberOfRecordsMatched}">
                    <xsl:value-of select="concat(count(atom:entry) + 1,
                        $token_sep, $metadataPrefix, $token_sep, '2', $token_sep, $until)"/>
                </resumptionToken>
            </xsl:when>
            <xsl:when test="$resumptionToken">
                <resumptionToken xmlns="http://www.openarchives.org/OAI/2.0/" cursor="{number(substring-before($resumptionToken, $token_sep)) - 1}" completeListSize="{$numberOfRecordsMatched}">
                    <!-- HACK: for some startIndex the ESA catalog does not return any collections, so always increase resumptionToken index by page size 10 -->
                    <xsl:if test="count(atom:entry) + number(substring-before($resumptionToken, $token_sep)) - 1 &lt; $numberOfRecordsMatched">
                        <xsl:value-of select="concat(10 + number(substring-before($resumptionToken, $token_sep)),
                            $token_sep, substring-before(substring-after($resumptionToken, $token_sep), $token_sep),
                            $token_sep, number(substring-before(substring-after(substring-after($resumptionToken, $token_sep), $token_sep), $token_sep)) + 1,
                            $token_sep, substring-after(substring-after(substring-after($resumptionToken, $token_sep), $token_sep), $token_sep))"/>
<!--
                        <xsl:value-of select="concat(count(atom:entry) + number(substring-before($resumptionToken, $token_sep)),
                            $token_sep, substring-before(substring-after($resumptionToken, $token_sep), $token_sep),
                            $token_sep, number(substring-before(substring-after(substring-after($resumptionToken, $token_sep), $token_sep), $token_sep)) + 1,
                            $token_sep, substring-after(substring-after(substring-after($resumptionToken, $token_sep), $token_sep), $token_sep))"/>
-->
                    </xsl:if>
                </resumptionToken>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="atom:feed[number(os:totalResult) &lt; 1]">
        <xsl:call-template name="error">
            <xsl:with-param name="errorCode">
                <xsl:choose>
                    <xsl:when test="$verb = 'GetRecord'">
                        <xsl:value-of select="'idDoesNotExist'"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="'noRecordsMatch'"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:with-param>
            <xsl:with-param name="errorMessage">The target catalog has no matching records</xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="gmd:MD_Metadata|gmi:MI_Metadata" mode="record">
        <record xmlns="http://www.openarchives.org/OAI/2.0/">
            <xsl:apply-templates mode="header" select="."/>
            <xsl:if test="$verb != 'ListIdentifiers'">
                <metadata>
                    <xsl:choose>
                        <xsl:when test="$metadataPrefix = $prefix_oai or contains($resumptionToken, $prefix_oai)">
                            <oai_dc:dc xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
                                    xmlns:dc="http://purl.org/dc/elements/1.1/"
                                    xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd">
                                <xsl:apply-templates select="gmd:identificationInfo/*/gmd:citation/*/gmd:title/*" mode="oai_dc"/>
                                <xsl:apply-templates select="gmd:identificationInfo/*/gmd:citation/*/gmd:citedResponsibleParty/*[gmd:role/gmd:CI_RoleCode/@codeListValue='originator']/gmd:organisationName/*" mode="oai_dc"/>
                                <xsl:apply-templates select="gmd:identificationInfo/*/gmd:descriptiveKeywords/*/gmd:keyword/*" mode="oai_dc"/>
                                <xsl:apply-templates select="gmd:identificationInfo//gmd:abstract/*" mode="oai_dc"/>
                                <xsl:apply-templates select="gmd:identificationInfo/*/gmd:citation/*/gmd:date/*[gmd:dateType/gmd:CI_DateTypeCode/@codeListValue='revision']/gmd:date/*" mode="oai_dc"/>
                                <xsl:apply-templates select="gmd:hierarchyLevel/gmd:MD_ScopeCode" mode="oai_dc"/>
                                <xsl:apply-templates select="gmd:distributionInfo/*/gmd:distributionFormat/*/gmd:name/*" mode="oai_dc"/>
                                <xsl:apply-templates select="gmd:fileIdentifier/*" mode="oai_dc"/>
                                <xsl:apply-templates select="gmd:identificationInfo/*/gmd:language/*" mode="oai_dc"/>
                                <xsl:apply-templates select="gmd:identificationInfo/*/*/*/gmd:geographicElement/*/gmd:geographicIdentifier/*/gmd:code/*" mode="oai_dc"/>
                                <xsl:apply-templates select="gmd:identificationInfo/*/gmd:resourceConstraints/gmd:MD_LegalConstraints/gmd:useLimitation/*|gmd:identificationInfo/*/gmd:resourceConstraints/gmd:MD_LegalConstraints/gmd:useConstraints/*/@codeListValue|gmd:identificationInfo/*/gmd:resourceConstraints/gmd:MD_LegalConstraints/gmd:otherConstraints/*" mode="oai_dc"/>
                            </oai_dc:dc>
                        </xsl:when>
                        <xsl:when test="$metadataPrefix = $prefix_iso19139 or contains($resumptionToken, $prefix_iso19139)">
                            <xsl:copy-of select="."/>
                        </xsl:when>
                        <xsl:otherwise>
                             <xsl:apply-templates select="."/>
                        </xsl:otherwise>
                    </xsl:choose>
                </metadata>
            </xsl:if>
        </record>
    </xsl:template>

    <xsl:template match="gmd:MD_Metadata|gmi:MI_Metadata" mode="header">
        <header xmlns="http://www.openarchives.org/OAI/2.0/">
            <identifier><xsl:value-of select="gmd:fileIdentifier/*"/></identifier>
            <datestamp>
                <xsl:variable name="dateStamp" select="gmd:dateStamp/*"/>
                <xsl:value-of select="$dateStamp"/>
                <xsl:if test="contains($dateStamp, 'T') and not(contains($dateStamp, 'Z'))">
                    <xsl:text>Z</xsl:text>
                </xsl:if>
            </datestamp>
        </header>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:citation/*/gmd:title/*" mode="oai_dc">
        <dc:title>
            <xsl:value-of select="."/>
        </dc:title>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:citation/*/gmd:citedResponsibleParty/*[gmd:role/gmd:CI_RoleCode/@codeListValue='originator']/gmd:organisationName/*" mode="oai_dc">
        <dc:creator>
            <xsl:value-of select="."/>
        </dc:creator>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:descriptiveKeywords/*/gmd:keyword/*" mode="oai_dc">
        <dc:subject>
            <xsl:value-of select="."/>
        </dc:subject>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo//gmd:abstract/*" mode="oai_dc">
        <dc:description>
            <xsl:value-of select="."/>
        </dc:description>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:citation/*/gmd:date/*[gmd:dateType/gmd:CI_DateTypeCode/@codeListValue='revision']/gmd:date/*" mode="oai_dc">
        <dc:date>
            <xsl:value-of select="."/>
        </dc:date>
    </xsl:template>

    <xsl:template match="gmd:hierarchyLevel/gmd:MD_ScopeCode" mode="oai_dc">
        <dc:type>
            <xsl:value-of select="@codeListValue"/>
        </dc:type>
    </xsl:template>

    <xsl:template match="gmd:distributionInfo/*/gmd:distributionFormat/*/gmd:name/*" mode="oai_dc">
        <dc:format>
            <xsl:value-of select="."/>
        </dc:format>
    </xsl:template>

    <xsl:template match="gmd:fileIdentifier/*" mode="oai_dc">
        <dc:identifier>
            <xsl:value-of select="."/>
        </dc:identifier>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:language/*" mode="oai_dc">
        <dc:language>
            <xsl:value-of select="@codeListValue"/>
        </dc:language>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/*/*/gmd:geographicElement/*/gmd:geographicIdentifier/*/gmd:code/*" mode="oai_dc">
        <dc:coverage>
            <xsl:value-of select="."/>
        </dc:coverage>
    </xsl:template>

    <xsl:template match="gmd:identificationInfo/*/gmd:resourceConstraints/*/gmd:useLimitation/*|gmd:identificationInfo/*/gmd:resourceConstraints/*/gmd:useConstraints/*/@codeListValue|gmd:identificationInfo/*/gmd:resourceConstraints/*/gmd:otherConstraints/*" mode="oai_dc">
        <dc:rights>
            <xsl:value-of select="."/>
        </dc:rights>
    </xsl:template>

</xsl:stylesheet>
